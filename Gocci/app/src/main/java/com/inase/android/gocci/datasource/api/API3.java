package com.inase.android.gocci.datasource.api;

import com.inase.android.gocci.Application_Gocci;
import com.inase.android.gocci.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface API3 {
    Util.UnsetSns_LinkLocalCode UnsetSns_LinkParameterRegex(String provider, String sns_token);

    void UnsetSns_LinkResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.UnsetDeviceLocalCode UnsetDeviceParameterRegex();

    void UnsetDeviceResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.UnsetFollowLocalCode UnsetFollowParameterRegex(String user_id);

    void UnsetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.UnsetCommentLocalCode UnsetCommentParameterRegex(String comment_id);

    void UnsetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.UnsetGochiLocalCode UnsetGochiParameterRegex(String post_id);

    void UnsetGochiResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.UnsetPostLocalCode UnsetPostParameterRegex(String post_id);

    void UnsetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetSns_LinkLocalCode SetSns_LinkParameterRegex(String provider, String sns_token);

    void SetSns_LinkResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetDeviceLocalCode SetDeviceParameterRegex(String device_token, String os, String ver, String model);

    void SetDeviceResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetPostLocalCode SetPostParameterRegex(String rest_id, String movie_name, String category_id, String value, String memo, String cheer_flag);

    void SetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetCommentLocalCode SetCommentParameterRegex(String post_id, String comment, String re_user_id);

    void SetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetComment_BlockLocalCode SetComment_BlockParameterRegex(String comment_id);

    void SetComment_BlockResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetRestLocalCode SetRestParameterRegex(String restname, String lat, String lon);

    void SetRestResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetPost_CrashLocalCode SetPost_CrashParameterRegex(String restname, String address, String movie_name, String category_id, String value, String memo, String cheer_flag);

    void SetPost_CrashResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetGochiLocalCode SetGochiParameterRegex(String post_id);

    void SetGochiResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetProfile_ImgLocalCode SetProfile_ImgParameterRegex(String profile_img);

    void SetProfile_ImgResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetPasswordLocalCode SetPasswordParameterRegex(String password);

    void SetPasswordResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetComment_EditLocalCode SetComment_EditParameterRegex(String comment_id, String comment);

    void SetComment_EditResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetFeedbackLocalCode SetFeedbackParameterRegex(String feedback);

    void SetFeedbackResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetFollowLocalCode SetFollowParameterRegex(String user_id);

    void SetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetMemo_EditLocalCode SetMemo_EditParameterRegex(String post_id, String memo);

    void SetMemo_EditResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetUsernameLocalCode SetUsernameParameterRegex(String username);

    void SetUsernameResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.SetPost_BlockLocalCode SetPost_BlockParameterRegex(String post_id);

    void SetPost_BlockResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.AuthLoginLocalCode AuthLoginParameterRegex(String identity_id);

    void AuthLoginResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.AuthSignupLocalCode AuthSignupParameterRegex(String username);

    void AuthSignupResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.AuthPasswordLocalCode AuthPasswordParameterRegex(String username, String password);

    void AuthPasswordResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetTimelineLocalCode GetTimelineParameterRegex(String page, String category_id, String value_id);

    void GetTimelineResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetNearLocalCode GetNearParameterRegex(String lat, String lon);

    void GetNearResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetNearlineLocalCode GetNearlineParameterRegex(String lat, String lon, String page, String category_id, String value_id);

    void GetNearlineResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetPostLocalCode GetPostParameterRegex(String post_id);

    void GetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetFollowlineLocalCode GetFollowlineParameterRegex(String page, String category_id, String value_id);

    void GetFollowlineResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetHeatmapLocalCode GetHeatmapParameterRegex();

    void GetHeatmapResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetFollower_RankLocalCode GetFollower_RankParameterRegex(String page);

    void GetFollower_RankResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetGochilineLocalCode GetGochilineParameterRegex(String page, String category_id, String value_id);

    void GetGochilineResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetFollowLocalCode GetFollowParameterRegex(String user_id);

    void GetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetUser_CheerLocalCode GetUser_CheerParameterRegex(String user_id);

    void GetUser_CheerResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetCommentLocalCode GetCommentParameterRegex(String post_id);

    void GetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetFollowerLocalCode GetFollowerParameterRegex(String user_id);

    void GetFollowerResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetUserLocalCode GetUserParameterRegex(String user_id);

    void GetUserResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetRestLocalCode GetRestParameterRegex(String rest_id);

    void GetRestResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetNoticeLocalCode GetNoticeParameterRegex();

    void GetNoticeResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    Util.GetUsernameLocalCode GetUsernameParameterRegex(String username);

    void GetUsernameResponse(JSONObject jsonObject, PayloadResponseCallback cb);

    interface PayloadResponseCallback {
        void onSuccess(JSONObject payload);

        void onGlobalError(Util.GlobalCode globalCode);

        void onLocalError(String errorMessage);
    }

    class Util {
        public static final String liveurl = "https://mobile.api.gocci.me/v4";
        public static final String testurl = "http://test.mobile.api.gocci.me/v4";
        public static final String version = "4.1";

        private static final ConcurrentHashMap<GlobalCode, String> GlobalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GlobalCode> GlobalCodeReverseMap = new ConcurrentHashMap<>();

        public enum GlobalCode {
            SUCCESS,
            ERROR_SESSION_EXPIRED,
            ERROR_CLIENT_OUTDATED,
            ERROR_UNKNOWN_ERROR,
        }

        public static GlobalCode GlobalCodeReverseLookupTable(String message) {
            if (GlobalCodeReverseMap.isEmpty()) {
                GlobalCodeReverseMap.put("ERROR_SESSION_EXPIRED", GlobalCode.ERROR_SESSION_EXPIRED);
                GlobalCodeReverseMap.put("SUCCESS", GlobalCode.SUCCESS);
                GlobalCodeReverseMap.put("ERROR_CLIENT_OUTDATED", GlobalCode.ERROR_CLIENT_OUTDATED);
                GlobalCodeReverseMap.put("ERROR_UNKNOWN_ERROR", GlobalCode.ERROR_UNKNOWN_ERROR);
            }
            GlobalCode code = null;
            for (Map.Entry<String, GlobalCode> entry : GlobalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        public static String GlobalCodeMessageTable(GlobalCode code) {
            if (GlobalCodeMap.isEmpty()) {
                GlobalCodeMap.put(GlobalCode.ERROR_SESSION_EXPIRED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GlobalCode_ERROR_SESSION_EXPIRED));
                GlobalCodeMap.put(GlobalCode.SUCCESS, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GlobalCode_SUCCESS));
                GlobalCodeMap.put(GlobalCode.ERROR_CLIENT_OUTDATED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GlobalCode_ERROR_CLIENT_OUTDATED));
                GlobalCodeMap.put(GlobalCode.ERROR_UNKNOWN_ERROR, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GlobalCode_ERROR_UNKNOWN_ERROR));
            }
            String message = null;
            for (Map.Entry<GlobalCode, String> entry : GlobalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        private static final ConcurrentHashMap<UnsetSns_LinkLocalCode, String> UnsetSns_LinkLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetSns_LinkLocalCode> UnsetSns_LinkLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetSnsLinkAPI(String provider, String sns_token) {
            StringBuilder url = new StringBuilder(testurl + "/unset/sns_link/");
            url.append("&provider=").append(provider);
            url.append("&sns_token=").append(sns_token);
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetSns_LinkLocalCode {
            ERROR_SNS_PROVIDER_TOKEN_NOT_VALID,
            ERROR_PROVIDER_UNREACHABLE,
            ERROR_PARAMETER_PROVIDER_MISSING,
            ERROR_PARAMETER_PROVIDER_MALFORMED,
            ERROR_PARAMETER_SNS_TOKEN_MISSING,
            ERROR_PARAMETER_SNS_TOKEN_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetSns_LinkLocalCodeMessageTable(UnsetSns_LinkLocalCode code) {
            if (UnsetSns_LinkLocalCodeMap.isEmpty()) {
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_PARAMETER_PROVIDER_MALFORMED));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_SNS_PROVIDER_TOKEN_NOT_VALID, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_SNS_PROVIDER_TOKEN_NOT_VALID));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_PARAMETER_SNS_TOKEN_MALFORMED));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_PROVIDER_UNREACHABLE, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_PROVIDER_UNREACHABLE));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_PARAMETER_SNS_TOKEN_MISSING));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_PARAMETER_PROVIDER_MISSING));
                UnsetSns_LinkLocalCodeMap.put(UnsetSns_LinkLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetSns_LinkLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<UnsetSns_LinkLocalCode, String> entry : UnsetSns_LinkLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetSns_LinkLocalCode UnsetSns_LinkLocalCodeReverseLookupTable(String message) {
            if (UnsetSns_LinkLocalCodeReverseMap.isEmpty()) {
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_PROVIDER_MALFORMED", UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_SNS_PROVIDER_TOKEN_NOT_VALID", UnsetSns_LinkLocalCode.ERROR_SNS_PROVIDER_TOKEN_NOT_VALID);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_SNS_TOKEN_MALFORMED", UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_PROVIDER_UNREACHABLE", UnsetSns_LinkLocalCode.ERROR_PROVIDER_UNREACHABLE);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_SNS_TOKEN_MISSING", UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_PROVIDER_MISSING", UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING);
                UnsetSns_LinkLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetSns_LinkLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            UnsetSns_LinkLocalCode code = null;
            for (Map.Entry<String, UnsetSns_LinkLocalCode> entry : UnsetSns_LinkLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<UnsetDeviceLocalCode, String> UnsetDeviceLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetDeviceLocalCode> UnsetDeviceLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetDeviceAPI() {
            StringBuilder url = new StringBuilder(testurl + "/unset/device/");
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetDeviceLocalCode {
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetDeviceLocalCodeMessageTable(UnsetDeviceLocalCode code) {
            if (UnsetDeviceLocalCodeMap.isEmpty()) {
                UnsetDeviceLocalCodeMap.put(UnsetDeviceLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetDeviceLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<UnsetDeviceLocalCode, String> entry : UnsetDeviceLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetDeviceLocalCode UnsetDeviceLocalCodeReverseLookupTable(String message) {
            if (UnsetDeviceLocalCodeReverseMap.isEmpty()) {
                UnsetDeviceLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetDeviceLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            UnsetDeviceLocalCode code = null;
            for (Map.Entry<String, UnsetDeviceLocalCode> entry : UnsetDeviceLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<UnsetFollowLocalCode, String> UnsetFollowLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetFollowLocalCode> UnsetFollowLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetFollowAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/unset/follow/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetFollowLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetFollowLocalCodeMessageTable(UnsetFollowLocalCode code) {
            if (UnsetFollowLocalCodeMap.isEmpty()) {
                UnsetFollowLocalCodeMap.put(UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetFollowLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                UnsetFollowLocalCodeMap.put(UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetFollowLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                UnsetFollowLocalCodeMap.put(UnsetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetFollowLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<UnsetFollowLocalCode, String> entry : UnsetFollowLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetFollowLocalCode UnsetFollowLocalCodeReverseLookupTable(String message) {
            if (UnsetFollowLocalCodeReverseMap.isEmpty()) {
                UnsetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                UnsetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                UnsetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            UnsetFollowLocalCode code = null;
            for (Map.Entry<String, UnsetFollowLocalCode> entry : UnsetFollowLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<UnsetCommentLocalCode, String> UnsetCommentLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetCommentLocalCode> UnsetCommentLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetCommentAPI(String comment_id) {
            StringBuilder url = new StringBuilder(testurl + "/unset/comment/");
            url.append("&comment_id=").append(comment_id);
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetCommentLocalCode {
            ERROR_PARAMETER_COMMENT_ID_MISSING,
            ERROR_PARAMETER_COMMENT_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetCommentLocalCodeMessageTable(UnsetCommentLocalCode code) {
            if (UnsetCommentLocalCodeMap.isEmpty()) {
                UnsetCommentLocalCodeMap.put(UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetCommentLocalCode_ERROR_PARAMETER_COMMENT_ID_MISSING));
                UnsetCommentLocalCodeMap.put(UnsetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetCommentLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                UnsetCommentLocalCodeMap.put(UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetCommentLocalCode_ERROR_PARAMETER_COMMENT_ID_MALFORMED));
            }
            String message = null;
            for (Map.Entry<UnsetCommentLocalCode, String> entry : UnsetCommentLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetCommentLocalCode UnsetCommentLocalCodeReverseLookupTable(String message) {
            if (UnsetCommentLocalCodeReverseMap.isEmpty()) {
                UnsetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MISSING", UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING);
                UnsetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                UnsetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MALFORMED", UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED);
            }
            UnsetCommentLocalCode code = null;
            for (Map.Entry<String, UnsetCommentLocalCode> entry : UnsetCommentLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<UnsetGochiLocalCode, String> UnsetGochiLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetGochiLocalCode> UnsetGochiLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetGochiAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/unset/gochi/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetGochiLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetGochiLocalCodeMessageTable(UnsetGochiLocalCode code) {
            if (UnsetGochiLocalCodeMap.isEmpty()) {
                UnsetGochiLocalCodeMap.put(UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetGochiLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                UnsetGochiLocalCodeMap.put(UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetGochiLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                UnsetGochiLocalCodeMap.put(UnsetGochiLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetGochiLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<UnsetGochiLocalCode, String> entry : UnsetGochiLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetGochiLocalCode UnsetGochiLocalCodeReverseLookupTable(String message) {
            if (UnsetGochiLocalCodeReverseMap.isEmpty()) {
                UnsetGochiLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                UnsetGochiLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                UnsetGochiLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetGochiLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            UnsetGochiLocalCode code = null;
            for (Map.Entry<String, UnsetGochiLocalCode> entry : UnsetGochiLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<UnsetPostLocalCode, String> UnsetPostLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, UnsetPostLocalCode> UnsetPostLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getUnsetPostAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/unset/post/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum UnsetPostLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String UnsetPostLocalCodeMessageTable(UnsetPostLocalCode code) {
            if (UnsetPostLocalCodeMap.isEmpty()) {
                UnsetPostLocalCodeMap.put(UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetPostLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                UnsetPostLocalCodeMap.put(UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetPostLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                UnsetPostLocalCodeMap.put(UnsetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.UnsetPostLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<UnsetPostLocalCode, String> entry : UnsetPostLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static UnsetPostLocalCode UnsetPostLocalCodeReverseLookupTable(String message) {
            if (UnsetPostLocalCodeReverseMap.isEmpty()) {
                UnsetPostLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                UnsetPostLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                UnsetPostLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", UnsetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            UnsetPostLocalCode code = null;
            for (Map.Entry<String, UnsetPostLocalCode> entry : UnsetPostLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetSns_LinkLocalCode, String> SetSns_LinkLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetSns_LinkLocalCode> SetSns_LinkLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetSnsLinkAPI(String provider, String sns_token) {
            StringBuilder url = new StringBuilder(testurl + "/set/sns_link/");
            url.append("&provider=").append(provider);
            url.append("&sns_token=").append(sns_token);
            return url.toString().replace("/&", "/?");
        }

        public enum SetSns_LinkLocalCode {
            ERROR_SNS_PROVIDER_TOKEN_NOT_VALID,
            ERROR_PROVIDER_UNREACHABLE,
            ERROR_PARAMETER_PROVIDER_MISSING,
            ERROR_PARAMETER_PROVIDER_MALFORMED,
            ERROR_PARAMETER_SNS_TOKEN_MISSING,
            ERROR_PARAMETER_SNS_TOKEN_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetSns_LinkLocalCodeMessageTable(SetSns_LinkLocalCode code) {
            if (SetSns_LinkLocalCodeMap.isEmpty()) {
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_PARAMETER_PROVIDER_MALFORMED));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_SNS_PROVIDER_TOKEN_NOT_VALID, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_SNS_PROVIDER_TOKEN_NOT_VALID));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_PARAMETER_SNS_TOKEN_MALFORMED));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_PROVIDER_UNREACHABLE, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_PROVIDER_UNREACHABLE));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_PARAMETER_SNS_TOKEN_MISSING));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_PARAMETER_PROVIDER_MISSING));
                SetSns_LinkLocalCodeMap.put(SetSns_LinkLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetSns_LinkLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetSns_LinkLocalCode, String> entry : SetSns_LinkLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetSns_LinkLocalCode SetSns_LinkLocalCodeReverseLookupTable(String message) {
            if (SetSns_LinkLocalCodeReverseMap.isEmpty()) {
                SetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_PROVIDER_MALFORMED", SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_SNS_PROVIDER_TOKEN_NOT_VALID", SetSns_LinkLocalCode.ERROR_SNS_PROVIDER_TOKEN_NOT_VALID);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_SNS_TOKEN_MALFORMED", SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_PROVIDER_UNREACHABLE", SetSns_LinkLocalCode.ERROR_PROVIDER_UNREACHABLE);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_SNS_TOKEN_MISSING", SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_PARAMETER_PROVIDER_MISSING", SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING);
                SetSns_LinkLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetSns_LinkLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetSns_LinkLocalCode code = null;
            for (Map.Entry<String, SetSns_LinkLocalCode> entry : SetSns_LinkLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetDeviceLocalCode, String> SetDeviceLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetDeviceLocalCode> SetDeviceLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetDeviceAPI(String device_token, String os, String ver, String model) {
            StringBuilder url = new StringBuilder(testurl + "/set/device/");
            url.append("&device_token=").append(device_token);
            url.append("&os=").append(os);
            url.append("&ver=").append(ver);
            url.append("&model=").append(model);
            return url.toString().replace("/&", "/?");
        }

        public enum SetDeviceLocalCode {
            ERROR_PARAMETER_DEVICE_TOKEN_MISSING,
            ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED,
            ERROR_PARAMETER_OS_MISSING,
            ERROR_PARAMETER_OS_MALFORMED,
            ERROR_PARAMETER_VER_MISSING,
            ERROR_PARAMETER_VER_MALFORMED,
            ERROR_PARAMETER_MODEL_MISSING,
            ERROR_PARAMETER_MODEL_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetDeviceLocalCodeMessageTable(SetDeviceLocalCode code) {
            if (SetDeviceLocalCodeMap.isEmpty()) {
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_DEVICE_TOKEN_MISSING));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_VER_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_VER_MALFORMED));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_OS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_OS_MISSING));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_MODEL_MISSING));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_MODEL_MALFORMED));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_OS_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_OS_MALFORMED));
                SetDeviceLocalCodeMap.put(SetDeviceLocalCode.ERROR_PARAMETER_VER_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetDeviceLocalCode_ERROR_PARAMETER_VER_MISSING));
            }
            String message = null;
            for (Map.Entry<SetDeviceLocalCode, String> entry : SetDeviceLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetDeviceLocalCode SetDeviceLocalCodeReverseLookupTable(String message) {
            if (SetDeviceLocalCodeReverseMap.isEmpty()) {
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_DEVICE_TOKEN_MISSING", SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MISSING);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_VER_MALFORMED", SetDeviceLocalCode.ERROR_PARAMETER_VER_MALFORMED);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_OS_MISSING", SetDeviceLocalCode.ERROR_PARAMETER_OS_MISSING);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_MODEL_MISSING", SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MISSING);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_MODEL_MALFORMED", SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MALFORMED);
                SetDeviceLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetDeviceLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED", SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_OS_MALFORMED", SetDeviceLocalCode.ERROR_PARAMETER_OS_MALFORMED);
                SetDeviceLocalCodeReverseMap.put("ERROR_PARAMETER_VER_MISSING", SetDeviceLocalCode.ERROR_PARAMETER_VER_MISSING);
            }
            SetDeviceLocalCode code = null;
            for (Map.Entry<String, SetDeviceLocalCode> entry : SetDeviceLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetPostLocalCode, String> SetPostLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetPostLocalCode> SetPostLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetPostAPI(String rest_id, String movie_name, String category_id, String value, String memo, String cheer_flag) {
            StringBuilder url = new StringBuilder(testurl + "/set/post/");
            url.append("&rest_id=").append(rest_id);
            url.append("&movie_name=").append(movie_name);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value != null) url.append("&value=").append(value);
            if (memo != null) url.append("&memo=").append(memo);
            if (cheer_flag != null) url.append("&cheer_flag=").append(cheer_flag);
            return url.toString().replace("/&", "/?");
        }

        public enum SetPostLocalCode {
            ERROR_PARAMETER_REST_ID_MISSING,
            ERROR_PARAMETER_REST_ID_MALFORMED,
            ERROR_PARAMETER_MOVIE_NAME_MISSING,
            ERROR_PARAMETER_MOVIE_NAME_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_MALFORMED,
            ERROR_PARAMETER_MEMO_MALFORMED,
            ERROR_PARAMETER_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POST_ID_MISSING,
            ERROR_RESPONSE_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetPostLocalCodeMessageTable(SetPostLocalCode code) {
            if (SetPostLocalCodeMap.isEmpty()) {
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_VALUE_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_MOVIE_NAME_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_CHEER_FLAG_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_REST_ID_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_RESPONSE_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_RESPONSE_POST_ID_MISSING));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_REST_ID_MISSING));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_MOVIE_NAME_MISSING));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_RESPONSE_POST_ID_MALFORMED));
                SetPostLocalCodeMap.put(SetPostLocalCode.ERROR_PARAMETER_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPostLocalCode_ERROR_PARAMETER_MEMO_MALFORMED));
            }
            String message = null;
            for (Map.Entry<SetPostLocalCode, String> entry : SetPostLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetPostLocalCode SetPostLocalCodeReverseLookupTable(String message) {
            if (SetPostLocalCodeReverseMap.isEmpty()) {
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_VALUE_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_MOVIE_NAME_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_CHEER_FLAG_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_REST_ID_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MISSING", SetPostLocalCode.ERROR_RESPONSE_POST_ID_MISSING);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_REST_ID_MISSING", SetPostLocalCode.ERROR_PARAMETER_REST_ID_MISSING);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_MOVIE_NAME_MISSING", SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING);
                SetPostLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MALFORMED", SetPostLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED);
                SetPostLocalCodeReverseMap.put("ERROR_PARAMETER_MEMO_MALFORMED", SetPostLocalCode.ERROR_PARAMETER_MEMO_MALFORMED);
            }
            SetPostLocalCode code = null;
            for (Map.Entry<String, SetPostLocalCode> entry : SetPostLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetCommentLocalCode, String> SetCommentLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetCommentLocalCode> SetCommentLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetCommentAPI(String post_id, String comment, String re_user_id) {
            StringBuilder url = new StringBuilder(testurl + "/set/comment/");
            url.append("&post_id=").append(post_id);
            url.append("&comment=").append(comment);
            if (re_user_id != null) url.append("&re_user_id=").append(re_user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum SetCommentLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_PARAMETER_COMMENT_MISSING,
            ERROR_PARAMETER_COMMENT_MALFORMED,
            ERROR_PARAMETER_RE_USER_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetCommentLocalCodeMessageTable(SetCommentLocalCode code) {
            if (SetCommentLocalCodeMap.isEmpty()) {
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_PARAMETER_RE_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_PARAMETER_RE_USER_ID_MALFORMED));
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_PARAMETER_COMMENT_MISSING));
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_PARAMETER_COMMENT_MALFORMED));
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                SetCommentLocalCodeMap.put(SetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetCommentLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetCommentLocalCode, String> entry : SetCommentLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetCommentLocalCode SetCommentLocalCodeReverseLookupTable(String message) {
            if (SetCommentLocalCodeReverseMap.isEmpty()) {
                SetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_RE_USER_ID_MALFORMED", SetCommentLocalCode.ERROR_PARAMETER_RE_USER_ID_MALFORMED);
                SetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_MISSING", SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MISSING);
                SetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_MALFORMED", SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED);
                SetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                SetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                SetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetCommentLocalCode code = null;
            for (Map.Entry<String, SetCommentLocalCode> entry : SetCommentLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetComment_BlockLocalCode, String> SetComment_BlockLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetComment_BlockLocalCode> SetComment_BlockLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetCommentBlockAPI(String comment_id) {
            StringBuilder url = new StringBuilder(testurl + "/set/comment_block/");
            url.append("&comment_id=").append(comment_id);
            return url.toString().replace("/&", "/?");
        }

        public enum SetComment_BlockLocalCode {
            ERROR_PARAMETER_COMMENT_ID_MISSING,
            ERROR_PARAMETER_COMMENT_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetComment_BlockLocalCodeMessageTable(SetComment_BlockLocalCode code) {
            if (SetComment_BlockLocalCodeMap.isEmpty()) {
                SetComment_BlockLocalCodeMap.put(SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_BlockLocalCode_ERROR_PARAMETER_COMMENT_ID_MISSING));
                SetComment_BlockLocalCodeMap.put(SetComment_BlockLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_BlockLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetComment_BlockLocalCodeMap.put(SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_BlockLocalCode_ERROR_PARAMETER_COMMENT_ID_MALFORMED));
            }
            String message = null;
            for (Map.Entry<SetComment_BlockLocalCode, String> entry : SetComment_BlockLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetComment_BlockLocalCode SetComment_BlockLocalCodeReverseLookupTable(String message) {
            if (SetComment_BlockLocalCodeReverseMap.isEmpty()) {
                SetComment_BlockLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MISSING", SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING);
                SetComment_BlockLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetComment_BlockLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetComment_BlockLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MALFORMED", SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED);
            }
            SetComment_BlockLocalCode code = null;
            for (Map.Entry<String, SetComment_BlockLocalCode> entry : SetComment_BlockLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetRestLocalCode, String> SetRestLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetRestLocalCode> SetRestLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetRestAPI(String restname, String lat, String lon) {
            StringBuilder url = new StringBuilder(testurl + "/set/rest/");
            url.append("&restname=").append(restname);
            url.append("&lat=").append(lat);
            url.append("&lon=").append(lon);
            return url.toString().replace("/&", "/?");
        }

        public enum SetRestLocalCode {
            ERROR_PARAMETER_RESTNAME_MISSING,
            ERROR_PARAMETER_RESTNAME_MALFORMED,
            ERROR_PARAMETER_LAT_MISSING,
            ERROR_PARAMETER_LAT_MALFORMED,
            ERROR_PARAMETER_LON_MISSING,
            ERROR_PARAMETER_LON_MALFORMED,
            ERROR_RESPONSE_REST_ID_MISSING,
            ERROR_RESPONSE_REST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetRestLocalCodeMessageTable(SetRestLocalCode code) {
            if (SetRestLocalCodeMap.isEmpty()) {
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_LAT_MALFORMED));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_RESPONSE_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_RESPONSE_REST_ID_MALFORMED));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_RESPONSE_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_RESPONSE_REST_ID_MISSING));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_LAT_MISSING));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_RESTNAME_MISSING));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_RESTNAME_MALFORMED));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_LON_MALFORMED));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetRestLocalCodeMap.put(SetRestLocalCode.ERROR_PARAMETER_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetRestLocalCode_ERROR_PARAMETER_LON_MISSING));
            }
            String message = null;
            for (Map.Entry<SetRestLocalCode, String> entry : SetRestLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetRestLocalCode SetRestLocalCodeReverseLookupTable(String message) {
            if (SetRestLocalCodeReverseMap.isEmpty()) {
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MALFORMED", SetRestLocalCode.ERROR_PARAMETER_LAT_MALFORMED);
                SetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_ID_MALFORMED", SetRestLocalCode.ERROR_RESPONSE_REST_ID_MALFORMED);
                SetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_ID_MISSING", SetRestLocalCode.ERROR_RESPONSE_REST_ID_MISSING);
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MISSING", SetRestLocalCode.ERROR_PARAMETER_LAT_MISSING);
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_RESTNAME_MISSING", SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MISSING);
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_RESTNAME_MALFORMED", SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED);
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MALFORMED", SetRestLocalCode.ERROR_PARAMETER_LON_MALFORMED);
                SetRestLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetRestLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetRestLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MISSING", SetRestLocalCode.ERROR_PARAMETER_LON_MISSING);
            }
            SetRestLocalCode code = null;
            for (Map.Entry<String, SetRestLocalCode> entry : SetRestLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetPost_CrashLocalCode, String> SetPost_CrashLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetPost_CrashLocalCode> SetPost_CrashLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetPostCrashAPI(String restname, String address, String movie_name, String category_id, String value, String memo, String cheer_flag) {
            StringBuilder url = new StringBuilder(testurl + "/set/post_crash/");
            url.append("&restname=").append(restname);
            url.append("&address=").append(address);
            url.append("&movie_name=").append(movie_name);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value != null) url.append("&value=").append(value);
            if (memo != null) url.append("&memo=").append(memo);
            if (cheer_flag != null) url.append("&cheer_flag=").append(cheer_flag);
            return url.toString().replace("/&", "/?");
        }

        public enum SetPost_CrashLocalCode {
            ERROR_PARAMETER_RESTNAME_MISSING,
            ERROR_PARAMETER_RESTNAME_MALFORMED,
            ERROR_PARAMETER_ADDRESS_MISSING,
            ERROR_PARAMETER_ADDRESS_MALFORMED,
            ERROR_PARAMETER_MOVIE_NAME_MISSING,
            ERROR_PARAMETER_MOVIE_NAME_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_MALFORMED,
            ERROR_PARAMETER_MEMO_MALFORMED,
            ERROR_PARAMETER_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POST_ID_MISSING,
            ERROR_RESPONSE_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetPost_CrashLocalCodeMessageTable(SetPost_CrashLocalCode code) {
            if (SetPost_CrashLocalCodeMap.isEmpty()) {
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_ADDRESS_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_RESPONSE_POST_ID_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_VALUE_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_RESTNAME_MISSING));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_RESTNAME_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_RESPONSE_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_RESPONSE_POST_ID_MISSING));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_ADDRESS_MISSING));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_MOVIE_NAME_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_CHEER_FLAG_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_MEMO_MALFORMED));
                SetPost_CrashLocalCodeMap.put(SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_CrashLocalCode_ERROR_PARAMETER_MOVIE_NAME_MISSING));
            }
            String message = null;
            for (Map.Entry<SetPost_CrashLocalCode, String> entry : SetPost_CrashLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetPost_CrashLocalCode SetPost_CrashLocalCodeReverseLookupTable(String message) {
            if (SetPost_CrashLocalCodeReverseMap.isEmpty()) {
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_ADDRESS_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MALFORMED", SetPost_CrashLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_VALUE_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_RESTNAME_MISSING", SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MISSING);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_RESTNAME_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MISSING", SetPost_CrashLocalCode.ERROR_RESPONSE_POST_ID_MISSING);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetPost_CrashLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_ADDRESS_MISSING", SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MISSING);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_MOVIE_NAME_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_CHEER_FLAG_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_MEMO_MALFORMED", SetPost_CrashLocalCode.ERROR_PARAMETER_MEMO_MALFORMED);
                SetPost_CrashLocalCodeReverseMap.put("ERROR_PARAMETER_MOVIE_NAME_MISSING", SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING);
            }
            SetPost_CrashLocalCode code = null;
            for (Map.Entry<String, SetPost_CrashLocalCode> entry : SetPost_CrashLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetGochiLocalCode, String> SetGochiLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetGochiLocalCode> SetGochiLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetGochiAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/set/gochi/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum SetGochiLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetGochiLocalCodeMessageTable(SetGochiLocalCode code) {
            if (SetGochiLocalCodeMap.isEmpty()) {
                SetGochiLocalCodeMap.put(SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetGochiLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                SetGochiLocalCodeMap.put(SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetGochiLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                SetGochiLocalCodeMap.put(SetGochiLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetGochiLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetGochiLocalCode, String> entry : SetGochiLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetGochiLocalCode SetGochiLocalCodeReverseLookupTable(String message) {
            if (SetGochiLocalCodeReverseMap.isEmpty()) {
                SetGochiLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                SetGochiLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                SetGochiLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetGochiLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetGochiLocalCode code = null;
            for (Map.Entry<String, SetGochiLocalCode> entry : SetGochiLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetProfile_ImgLocalCode, String> SetProfile_ImgLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetProfile_ImgLocalCode> SetProfile_ImgLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetProfileImgAPI(String profile_img) {
            StringBuilder url = new StringBuilder(testurl + "/set/profile_img/");
            url.append("&profile_img=").append(profile_img);
            return url.toString().replace("/&", "/?");
        }

        public enum SetProfile_ImgLocalCode {
            ERROR_PARAMETER_PROFILE_IMG_MISSING,
            ERROR_PARAMETER_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetProfile_ImgLocalCodeMessageTable(SetProfile_ImgLocalCode code) {
            if (SetProfile_ImgLocalCodeMap.isEmpty()) {
                SetProfile_ImgLocalCodeMap.put(SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetProfile_ImgLocalCode_ERROR_PARAMETER_PROFILE_IMG_MALFORMED));
                SetProfile_ImgLocalCodeMap.put(SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetProfile_ImgLocalCode_ERROR_PARAMETER_PROFILE_IMG_MISSING));
                SetProfile_ImgLocalCodeMap.put(SetProfile_ImgLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetProfile_ImgLocalCode_ERROR_RESPONSE_PROFILE_IMG_MALFORMED));
                SetProfile_ImgLocalCodeMap.put(SetProfile_ImgLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetProfile_ImgLocalCode_ERROR_RESPONSE_PROFILE_IMG_MISSING));
                SetProfile_ImgLocalCodeMap.put(SetProfile_ImgLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetProfile_ImgLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetProfile_ImgLocalCode, String> entry : SetProfile_ImgLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetProfile_ImgLocalCode SetProfile_ImgLocalCodeReverseLookupTable(String message) {
            if (SetProfile_ImgLocalCodeReverseMap.isEmpty()) {
                SetProfile_ImgLocalCodeReverseMap.put("ERROR_PARAMETER_PROFILE_IMG_MALFORMED", SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MALFORMED);
                SetProfile_ImgLocalCodeReverseMap.put("ERROR_PARAMETER_PROFILE_IMG_MISSING", SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MISSING);
                SetProfile_ImgLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MALFORMED", SetProfile_ImgLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED);
                SetProfile_ImgLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MISSING", SetProfile_ImgLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING);
                SetProfile_ImgLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetProfile_ImgLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetProfile_ImgLocalCode code = null;
            for (Map.Entry<String, SetProfile_ImgLocalCode> entry : SetProfile_ImgLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetPasswordLocalCode, String> SetPasswordLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetPasswordLocalCode> SetPasswordLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetPasswordAPI(String password) {
            StringBuilder url = new StringBuilder(testurl + "/set/password/");
            url.append("&password=").append(password);
            return url.toString().replace("/&", "/?");
        }

        public enum SetPasswordLocalCode {
            ERROR_PARAMETER_PASSWORD_MISSING,
            ERROR_PARAMETER_PASSWORD_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetPasswordLocalCodeMessageTable(SetPasswordLocalCode code) {
            if (SetPasswordLocalCodeMap.isEmpty()) {
                SetPasswordLocalCodeMap.put(SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPasswordLocalCode_ERROR_PARAMETER_PASSWORD_MISSING));
                SetPasswordLocalCodeMap.put(SetPasswordLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPasswordLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetPasswordLocalCodeMap.put(SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPasswordLocalCode_ERROR_PARAMETER_PASSWORD_MALFORMED));
            }
            String message = null;
            for (Map.Entry<SetPasswordLocalCode, String> entry : SetPasswordLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetPasswordLocalCode SetPasswordLocalCodeReverseLookupTable(String message) {
            if (SetPasswordLocalCodeReverseMap.isEmpty()) {
                SetPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_PASSWORD_MISSING", SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING);
                SetPasswordLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetPasswordLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_PASSWORD_MALFORMED", SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED);
            }
            SetPasswordLocalCode code = null;
            for (Map.Entry<String, SetPasswordLocalCode> entry : SetPasswordLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetComment_EditLocalCode, String> SetComment_EditLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetComment_EditLocalCode> SetComment_EditLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetCommentEditAPI(String comment_id, String comment) {
            StringBuilder url = new StringBuilder(testurl + "/set/comment_edit/");
            url.append("&comment_id=").append(comment_id);
            url.append("&comment=").append(comment);
            return url.toString().replace("/&", "/?");
        }

        public enum SetComment_EditLocalCode {
            ERROR_PARAMETER_COMMENT_ID_MISSING,
            ERROR_PARAMETER_COMMENT_ID_MALFORMED,
            ERROR_PARAMETER_COMMENT_MISSING,
            ERROR_PARAMETER_COMMENT_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetComment_EditLocalCodeMessageTable(SetComment_EditLocalCode code) {
            if (SetComment_EditLocalCodeMap.isEmpty()) {
                SetComment_EditLocalCodeMap.put(SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_EditLocalCode_ERROR_PARAMETER_COMMENT_MISSING));
                SetComment_EditLocalCodeMap.put(SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_EditLocalCode_ERROR_PARAMETER_COMMENT_MALFORMED));
                SetComment_EditLocalCodeMap.put(SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_EditLocalCode_ERROR_PARAMETER_COMMENT_ID_MISSING));
                SetComment_EditLocalCodeMap.put(SetComment_EditLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_EditLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetComment_EditLocalCodeMap.put(SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetComment_EditLocalCode_ERROR_PARAMETER_COMMENT_ID_MALFORMED));
            }
            String message = null;
            for (Map.Entry<SetComment_EditLocalCode, String> entry : SetComment_EditLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetComment_EditLocalCode SetComment_EditLocalCodeReverseLookupTable(String message) {
            if (SetComment_EditLocalCodeReverseMap.isEmpty()) {
                SetComment_EditLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_MISSING", SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MISSING);
                SetComment_EditLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_MALFORMED", SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED);
                SetComment_EditLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MISSING", SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING);
                SetComment_EditLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetComment_EditLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetComment_EditLocalCodeReverseMap.put("ERROR_PARAMETER_COMMENT_ID_MALFORMED", SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED);
            }
            SetComment_EditLocalCode code = null;
            for (Map.Entry<String, SetComment_EditLocalCode> entry : SetComment_EditLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetFeedbackLocalCode, String> SetFeedbackLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetFeedbackLocalCode> SetFeedbackLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetFeedbackAPI(String feedback) {
            StringBuilder url = new StringBuilder(testurl + "/set/feedback/");
            url.append("&feedback=").append(feedback);
            return url.toString().replace("/&", "/?");
        }

        public enum SetFeedbackLocalCode {
            ERROR_PARAMETER_FEEDBACK_MISSING,
            ERROR_PARAMETER_FEEDBACK_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetFeedbackLocalCodeMessageTable(SetFeedbackLocalCode code) {
            if (SetFeedbackLocalCodeMap.isEmpty()) {
                SetFeedbackLocalCodeMap.put(SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFeedbackLocalCode_ERROR_PARAMETER_FEEDBACK_MALFORMED));
                SetFeedbackLocalCodeMap.put(SetFeedbackLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFeedbackLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetFeedbackLocalCodeMap.put(SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFeedbackLocalCode_ERROR_PARAMETER_FEEDBACK_MISSING));
            }
            String message = null;
            for (Map.Entry<SetFeedbackLocalCode, String> entry : SetFeedbackLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetFeedbackLocalCode SetFeedbackLocalCodeReverseLookupTable(String message) {
            if (SetFeedbackLocalCodeReverseMap.isEmpty()) {
                SetFeedbackLocalCodeReverseMap.put("ERROR_PARAMETER_FEEDBACK_MALFORMED", SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MALFORMED);
                SetFeedbackLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetFeedbackLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetFeedbackLocalCodeReverseMap.put("ERROR_PARAMETER_FEEDBACK_MISSING", SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MISSING);
            }
            SetFeedbackLocalCode code = null;
            for (Map.Entry<String, SetFeedbackLocalCode> entry : SetFeedbackLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetFollowLocalCode, String> SetFollowLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetFollowLocalCode> SetFollowLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetFollowAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/set/follow/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum SetFollowLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetFollowLocalCodeMessageTable(SetFollowLocalCode code) {
            if (SetFollowLocalCodeMap.isEmpty()) {
                SetFollowLocalCodeMap.put(SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFollowLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                SetFollowLocalCodeMap.put(SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFollowLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                SetFollowLocalCodeMap.put(SetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetFollowLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetFollowLocalCode, String> entry : SetFollowLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetFollowLocalCode SetFollowLocalCodeReverseLookupTable(String message) {
            if (SetFollowLocalCodeReverseMap.isEmpty()) {
                SetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                SetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                SetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetFollowLocalCode code = null;
            for (Map.Entry<String, SetFollowLocalCode> entry : SetFollowLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetMemo_EditLocalCode, String> SetMemo_EditLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetMemo_EditLocalCode> SetMemo_EditLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetMemoEditAPI(String post_id, String memo) {
            StringBuilder url = new StringBuilder(testurl + "/set/memo_edit/");
            url.append("&post_id=").append(post_id);
            url.append("&memo=").append(memo);
            return url.toString().replace("/&", "/?");
        }

        public enum SetMemo_EditLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_PARAMETER_MEMO_MISSING,
            ERROR_PARAMETER_MEMO_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetMemo_EditLocalCodeMessageTable(SetMemo_EditLocalCode code) {
            if (SetMemo_EditLocalCodeMap.isEmpty()) {
                SetMemo_EditLocalCodeMap.put(SetMemo_EditLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetMemo_EditLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                SetMemo_EditLocalCodeMap.put(SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetMemo_EditLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                SetMemo_EditLocalCodeMap.put(SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetMemo_EditLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                SetMemo_EditLocalCodeMap.put(SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetMemo_EditLocalCode_ERROR_PARAMETER_MEMO_MISSING));
                SetMemo_EditLocalCodeMap.put(SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetMemo_EditLocalCode_ERROR_PARAMETER_MEMO_MALFORMED));
            }
            String message = null;
            for (Map.Entry<SetMemo_EditLocalCode, String> entry : SetMemo_EditLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetMemo_EditLocalCode SetMemo_EditLocalCodeReverseLookupTable(String message) {
            if (SetMemo_EditLocalCodeReverseMap.isEmpty()) {
                SetMemo_EditLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetMemo_EditLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                SetMemo_EditLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                SetMemo_EditLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                SetMemo_EditLocalCodeReverseMap.put("ERROR_PARAMETER_MEMO_MISSING", SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MISSING);
                SetMemo_EditLocalCodeReverseMap.put("ERROR_PARAMETER_MEMO_MALFORMED", SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MALFORMED);
            }
            SetMemo_EditLocalCode code = null;
            for (Map.Entry<String, SetMemo_EditLocalCode> entry : SetMemo_EditLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetUsernameLocalCode, String> SetUsernameLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetUsernameLocalCode> SetUsernameLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetUsernameAPI(String username) {
            StringBuilder url = new StringBuilder(testurl + "/set/username/");
            url.append("&username=").append(username);
            return url.toString().replace("/&", "/?");
        }

        public enum SetUsernameLocalCode {
            ERROR_USERNAME_ALREADY_REGISTERD,
            ERROR_PARAMETER_USERNAME_MISSING,
            ERROR_PARAMETER_USERNAME_MALFORMED,
            ERROR_RESPONSE_USERNAME_MISSING,
            ERROR_RESPONSE_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetUsernameLocalCodeMessageTable(SetUsernameLocalCode code) {
            if (SetUsernameLocalCodeMap.isEmpty()) {
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_USERNAME_ALREADY_REGISTERD, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_USERNAME_ALREADY_REGISTERD));
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_RESPONSE_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_RESPONSE_USERNAME_MISSING));
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_RESPONSE_USERNAME_MALFORMED));
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_PARAMETER_USERNAME_MISSING));
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_PARAMETER_USERNAME_MALFORMED));
                SetUsernameLocalCodeMap.put(SetUsernameLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetUsernameLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetUsernameLocalCode, String> entry : SetUsernameLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetUsernameLocalCode SetUsernameLocalCodeReverseLookupTable(String message) {
            if (SetUsernameLocalCodeReverseMap.isEmpty()) {
                SetUsernameLocalCodeReverseMap.put("ERROR_USERNAME_ALREADY_REGISTERD", SetUsernameLocalCode.ERROR_USERNAME_ALREADY_REGISTERD);
                SetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MISSING", SetUsernameLocalCode.ERROR_RESPONSE_USERNAME_MISSING);
                SetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MALFORMED", SetUsernameLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED);
                SetUsernameLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MISSING", SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING);
                SetUsernameLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MALFORMED", SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED);
                SetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetUsernameLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetUsernameLocalCode code = null;
            for (Map.Entry<String, SetUsernameLocalCode> entry : SetUsernameLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<SetPost_BlockLocalCode, String> SetPost_BlockLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, SetPost_BlockLocalCode> SetPost_BlockLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getSetPostBlockAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/set/post_block/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum SetPost_BlockLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String SetPost_BlockLocalCodeMessageTable(SetPost_BlockLocalCode code) {
            if (SetPost_BlockLocalCodeMap.isEmpty()) {
                SetPost_BlockLocalCodeMap.put(SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_BlockLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                SetPost_BlockLocalCodeMap.put(SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_BlockLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                SetPost_BlockLocalCodeMap.put(SetPost_BlockLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.SetPost_BlockLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<SetPost_BlockLocalCode, String> entry : SetPost_BlockLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static SetPost_BlockLocalCode SetPost_BlockLocalCodeReverseLookupTable(String message) {
            if (SetPost_BlockLocalCodeReverseMap.isEmpty()) {
                SetPost_BlockLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                SetPost_BlockLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                SetPost_BlockLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", SetPost_BlockLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            SetPost_BlockLocalCode code = null;
            for (Map.Entry<String, SetPost_BlockLocalCode> entry : SetPost_BlockLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<AuthLoginLocalCode, String> AuthLoginLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, AuthLoginLocalCode> AuthLoginLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getAuthLoginAPI(String identity_id) {
            StringBuilder url = new StringBuilder(testurl + "/auth/login/");
            url.append("&identity_id=").append(identity_id);
            return url.toString().replace("/&", "/?");
        }

        public enum AuthLoginLocalCode {
            ERROR_IDENTITY_ID_NOT_REGISTERD,
            ERROR_PARAMETER_IDENTITY_ID_MISSING,
            ERROR_PARAMETER_IDENTITY_ID_MALFORMED,
            ERROR_RESPONSE_USER_ID_MISSING,
            ERROR_RESPONSE_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERNAME_MISSING,
            ERROR_RESPONSE_USERNAME_MALFORMED,
            ERROR_RESPONSE_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_BADGE_NUM_MISSING,
            ERROR_RESPONSE_BADGE_NUM_MALFORMED,
            ERROR_RESPONSE_IDENTITY_ID_MISSING,
            ERROR_RESPONSE_IDENTITY_ID_MALFORMED,
            ERROR_RESPONSE_COGNITO_TOKEN_MISSING,
            ERROR_RESPONSE_COGNITO_TOKEN_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String AuthLoginLocalCodeMessageTable(AuthLoginLocalCode code) {
            if (AuthLoginLocalCodeMap.isEmpty()) {
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_COGNITO_TOKEN_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_COGNITO_TOKEN_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_PARAMETER_IDENTITY_ID_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_USERNAME_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_IDENTITY_ID_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_IDENTITY_ID_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_USERNAME_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_IDENTITY_ID_NOT_REGISTERD, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_IDENTITY_ID_NOT_REGISTERD));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_PROFILE_IMG_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_BADGE_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_BADGE_NUM_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_COGNITO_TOKEN_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_COGNITO_TOKEN_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_USER_ID_MISSING));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_USER_ID_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_PARAMETER_IDENTITY_ID_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_PROFILE_IMG_MALFORMED));
                AuthLoginLocalCodeMap.put(AuthLoginLocalCode.ERROR_RESPONSE_BADGE_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthLoginLocalCode_ERROR_RESPONSE_BADGE_NUM_MISSING));
            }
            String message = null;
            for (Map.Entry<AuthLoginLocalCode, String> entry : AuthLoginLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static AuthLoginLocalCode AuthLoginLocalCodeReverseLookupTable(String message) {
            if (AuthLoginLocalCodeReverseMap.isEmpty()) {
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_COGNITO_TOKEN_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_COGNITO_TOKEN_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_PARAMETER_IDENTITY_ID_MISSING", AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_USERNAME_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_IDENTITY_ID_NOT_REGISTERD", AuthLoginLocalCode.ERROR_IDENTITY_ID_NOT_REGISTERD);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_BADGE_NUM_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_BADGE_NUM_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_COGNITO_TOKEN_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_COGNITO_TOKEN_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_USER_ID_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_USER_ID_MISSING);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_USER_ID_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_USER_ID_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_PARAMETER_IDENTITY_ID_MALFORMED", AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MALFORMED", AuthLoginLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED);
                AuthLoginLocalCodeReverseMap.put("ERROR_RESPONSE_BADGE_NUM_MISSING", AuthLoginLocalCode.ERROR_RESPONSE_BADGE_NUM_MISSING);
            }
            AuthLoginLocalCode code = null;
            for (Map.Entry<String, AuthLoginLocalCode> entry : AuthLoginLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<AuthSignupLocalCode, String> AuthSignupLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, AuthSignupLocalCode> AuthSignupLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getAuthSignupAPI(String username) {
            StringBuilder url = new StringBuilder(testurl + "/auth/signup/");
            url.append("&username=").append(username);
            return url.toString().replace("/&", "/?");
        }

        public enum AuthSignupLocalCode {
            ERROR_USERNAME_ALREADY_REGISTERD,
            ERROR_PARAMETER_USERNAME_MISSING,
            ERROR_PARAMETER_USERNAME_MALFORMED,
            ERROR_RESPONSE_IDENTITY_ID_MISSING,
            ERROR_RESPONSE_IDENTITY_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String AuthSignupLocalCodeMessageTable(AuthSignupLocalCode code) {
            if (AuthSignupLocalCodeMap.isEmpty()) {
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_USERNAME_ALREADY_REGISTERD, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_USERNAME_ALREADY_REGISTERD));
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_RESPONSE_IDENTITY_ID_MISSING));
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_RESPONSE_IDENTITY_ID_MALFORMED));
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_PARAMETER_USERNAME_MISSING));
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_PARAMETER_USERNAME_MALFORMED));
                AuthSignupLocalCodeMap.put(AuthSignupLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthSignupLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<AuthSignupLocalCode, String> entry : AuthSignupLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static AuthSignupLocalCode AuthSignupLocalCodeReverseLookupTable(String message) {
            if (AuthSignupLocalCodeReverseMap.isEmpty()) {
                AuthSignupLocalCodeReverseMap.put("ERROR_USERNAME_ALREADY_REGISTERD", AuthSignupLocalCode.ERROR_USERNAME_ALREADY_REGISTERD);
                AuthSignupLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MISSING", AuthSignupLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING);
                AuthSignupLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MALFORMED", AuthSignupLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED);
                AuthSignupLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MISSING", AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MISSING);
                AuthSignupLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MALFORMED", AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED);
                AuthSignupLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", AuthSignupLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            AuthSignupLocalCode code = null;
            for (Map.Entry<String, AuthSignupLocalCode> entry : AuthSignupLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<AuthPasswordLocalCode, String> AuthPasswordLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, AuthPasswordLocalCode> AuthPasswordLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getAuthPasswordAPI(String username, String password) {
            StringBuilder url = new StringBuilder(testurl + "/auth/password/");
            url.append("&username=").append(username);
            url.append("&password=").append(password);
            return url.toString().replace("/&", "/?");
        }

        public enum AuthPasswordLocalCode {
            ERROR_USERNAME_NOT_REGISTERD,
            ERROR_PASSWORD_NOT_REGISTERD,
            ERROR_PASSWORD_WRONG,
            ERROR_PARAMETER_USERNAME_MISSING,
            ERROR_PARAMETER_USERNAME_MALFORMED,
            ERROR_PARAMETER_PASSWORD_MISSING,
            ERROR_PARAMETER_PASSWORD_MALFORMED,
            ERROR_RESPONSE_IDENTITY_ID_MISSING,
            ERROR_RESPONSE_IDENTITY_ID_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String AuthPasswordLocalCodeMessageTable(AuthPasswordLocalCode code) {
            if (AuthPasswordLocalCodeMap.isEmpty()) {
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PARAMETER_PASSWORD_MISSING));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PASSWORD_WRONG, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PASSWORD_WRONG));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_RESPONSE_IDENTITY_ID_MISSING));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_USERNAME_NOT_REGISTERD, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_USERNAME_NOT_REGISTERD));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PARAMETER_USERNAME_MISSING));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PASSWORD_NOT_REGISTERD, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PASSWORD_NOT_REGISTERD));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PARAMETER_USERNAME_MALFORMED));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_RESPONSE_IDENTITY_ID_MALFORMED));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_PARAMETER_PASSWORD_MALFORMED));
                AuthPasswordLocalCodeMap.put(AuthPasswordLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.AuthPasswordLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<AuthPasswordLocalCode, String> entry : AuthPasswordLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static AuthPasswordLocalCode AuthPasswordLocalCodeReverseLookupTable(String message) {
            if (AuthPasswordLocalCodeReverseMap.isEmpty()) {
                AuthPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_PASSWORD_MISSING", AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING);
                AuthPasswordLocalCodeReverseMap.put("ERROR_PASSWORD_WRONG", AuthPasswordLocalCode.ERROR_PASSWORD_WRONG);
                AuthPasswordLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MISSING", AuthPasswordLocalCode.ERROR_RESPONSE_IDENTITY_ID_MISSING);
                AuthPasswordLocalCodeReverseMap.put("ERROR_USERNAME_NOT_REGISTERD", AuthPasswordLocalCode.ERROR_USERNAME_NOT_REGISTERD);
                AuthPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MISSING", AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MISSING);
                AuthPasswordLocalCodeReverseMap.put("ERROR_PASSWORD_NOT_REGISTERD", AuthPasswordLocalCode.ERROR_PASSWORD_NOT_REGISTERD);
                AuthPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MALFORMED", AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED);
                AuthPasswordLocalCodeReverseMap.put("ERROR_RESPONSE_IDENTITY_ID_MALFORMED", AuthPasswordLocalCode.ERROR_RESPONSE_IDENTITY_ID_MALFORMED);
                AuthPasswordLocalCodeReverseMap.put("ERROR_PARAMETER_PASSWORD_MALFORMED", AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED);
                AuthPasswordLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", AuthPasswordLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            AuthPasswordLocalCode code = null;
            for (Map.Entry<String, AuthPasswordLocalCode> entry : AuthPasswordLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetTimelineLocalCode, String> GetTimelineLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetTimelineLocalCode> GetTimelineLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetTimelineAPI(String page, String category_id, String value_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/timeline/");
            if (page != null) url.append("&page=").append(page);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value_id != null) url.append("&value_id=").append(value_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetTimelineLocalCode {
            ERROR_PARAMETER_PAGE_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_RESTNAME_MISSING,
            ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_USER_ID_MISSING,
            ERROR_RESPONSE_POSTS_USER_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_USERNAME_MISSING,
            ERROR_RESPONSE_POSTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetTimelineLocalCodeMessageTable(GetTimelineLocalCode code) {
            if (GetTimelineLocalCodeMap.isEmpty()) {
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_PARAMETER_PAGE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_PARAMETER_VALUE_ID_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_MISSING));
                GetTimelineLocalCodeMap.put(GetTimelineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetTimelineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING));
            }
            String message = null;
            for (Map.Entry<GetTimelineLocalCode, String> entry : GetTimelineLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetTimelineLocalCode GetTimelineLocalCodeReverseLookupTable(String message) {
            if (GetTimelineLocalCodeReverseMap.isEmpty()) {
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", GetTimelineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_PARAMETER_PAGE_MALFORMED", GetTimelineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_ID_MALFORMED", GetTimelineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_MISSING);
                GetTimelineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING", GetTimelineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING);
            }
            GetTimelineLocalCode code = null;
            for (Map.Entry<String, GetTimelineLocalCode> entry : GetTimelineLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetNearLocalCode, String> GetNearLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetNearLocalCode> GetNearLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetNearAPI(String lat, String lon) {
            StringBuilder url = new StringBuilder(testurl + "/get/near/");
            url.append("&lat=").append(lat);
            url.append("&lon=").append(lon);
            return url.toString().replace("/&", "/?");
        }

        public enum GetNearLocalCode {
            ERROR_PARAMETER_LAT_MISSING,
            ERROR_PARAMETER_LAT_MALFORMED,
            ERROR_PARAMETER_LON_MISSING,
            ERROR_PARAMETER_LON_MALFORMED,
            ERROR_RESPONSE_RESTS_MISSING,
            ERROR_RESPONSE_RESTS_REST_ID_MISSING,
            ERROR_RESPONSE_RESTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_RESTS_RESTNAME_MISSING,
            ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetNearLocalCodeMessageTable(GetNearLocalCode code) {
            if (GetNearLocalCodeMap.isEmpty()) {
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_PARAMETER_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_PARAMETER_LAT_MALFORMED));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MALFORMED));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MISSING));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_RESTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_RESTS_MISSING));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_PARAMETER_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_PARAMETER_LON_MALFORMED));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MISSING));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_PARAMETER_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_PARAMETER_LAT_MISSING));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetNearLocalCodeMap.put(GetNearLocalCode.ERROR_PARAMETER_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearLocalCode_ERROR_PARAMETER_LON_MISSING));
            }
            String message = null;
            for (Map.Entry<GetNearLocalCode, String> entry : GetNearLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetNearLocalCode GetNearLocalCodeReverseLookupTable(String message) {
            if (GetNearLocalCodeReverseMap.isEmpty()) {
                GetNearLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MALFORMED", GetNearLocalCode.ERROR_PARAMETER_LAT_MALFORMED);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MALFORMED", GetNearLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MISSING", GetNearLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_MISSING", GetNearLocalCode.ERROR_RESPONSE_RESTS_MISSING);
                GetNearLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MALFORMED", GetNearLocalCode.ERROR_PARAMETER_LON_MALFORMED);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED", GetNearLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MISSING", GetNearLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING);
                GetNearLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MISSING", GetNearLocalCode.ERROR_PARAMETER_LAT_MISSING);
                GetNearLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetNearLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetNearLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MISSING", GetNearLocalCode.ERROR_PARAMETER_LON_MISSING);
            }
            GetNearLocalCode code = null;
            for (Map.Entry<String, GetNearLocalCode> entry : GetNearLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetNearlineLocalCode, String> GetNearlineLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetNearlineLocalCode> GetNearlineLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetNearlineAPI(String lat, String lon, String page, String category_id, String value_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/nearline/");
            url.append("&lat=").append(lat);
            url.append("&lon=").append(lon);
            if (page != null) url.append("&page=").append(page);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value_id != null) url.append("&value_id=").append(value_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetNearlineLocalCode {
            ERROR_PARAMETER_LAT_MISSING,
            ERROR_PARAMETER_LAT_MALFORMED,
            ERROR_PARAMETER_LON_MISSING,
            ERROR_PARAMETER_LON_MALFORMED,
            ERROR_PARAMETER_PAGE_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_DISTANCE_MISSING,
            ERROR_RESPONSE_POSTS_DISTANCE_MALFORMED,
            ERROR_RESPONSE_POSTS_LAT_MISSING,
            ERROR_RESPONSE_POSTS_LAT_MALFORMED,
            ERROR_RESPONSE_POSTS_LON_MISSING,
            ERROR_RESPONSE_POSTS_LON_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_RESTNAME_MISSING,
            ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_USER_ID_MISSING,
            ERROR_RESPONSE_POSTS_USER_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_USERNAME_MISSING,
            ERROR_RESPONSE_POSTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetNearlineLocalCodeMessageTable(GetNearlineLocalCode code) {
            if (GetNearlineLocalCodeMap.isEmpty()) {
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_LON_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_LAT_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_DISTANCE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_DISTANCE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_LAT_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_LON_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_PAGE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_LON_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_LAT_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_LON_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_DISTANCE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_DISTANCE_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_LAT_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_PARAMETER_VALUE_ID_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_MISSING));
                GetNearlineLocalCodeMap.put(GetNearlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNearlineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING));
            }
            String message = null;
            for (Map.Entry<GetNearlineLocalCode, String> entry : GetNearlineLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetNearlineLocalCode GetNearlineLocalCodeReverseLookupTable(String message) {
            if (GetNearlineLocalCodeReverseMap.isEmpty()) {
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LON_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LON_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LAT_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LAT_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_DISTANCE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_DISTANCE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LAT_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LAT_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", GetNearlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LON_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_LON_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_PAGE_MALFORMED", GetNearlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MISSING", GetNearlineLocalCode.ERROR_PARAMETER_LON_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MALFORMED", GetNearlineLocalCode.ERROR_PARAMETER_LAT_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_LON_MALFORMED", GetNearlineLocalCode.ERROR_PARAMETER_LON_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_DISTANCE_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_DISTANCE_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_LAT_MISSING", GetNearlineLocalCode.ERROR_PARAMETER_LAT_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_ID_MALFORMED", GetNearlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_MISSING);
                GetNearlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING", GetNearlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING);
            }
            GetNearlineLocalCode code = null;
            for (Map.Entry<String, GetNearlineLocalCode> entry : GetNearlineLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetPostLocalCode, String> GetPostLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetPostLocalCode> GetPostLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetPostAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/post/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetPostLocalCode {
            ERROR_POST_DOES_NOT_EXIST,
            ERROR_POST_WAS_NEVER_COMPLETED,
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_CATEGORY_MISSING,
            ERROR_RESPONSE_CATEGORY_MALFORMED,
            ERROR_RESPONSE_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_COMMENT_NUM_MISSING,
            ERROR_RESPONSE_COMMENT_NUM_MALFORMED,
            ERROR_RESPONSE_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_MEMO_MISSING,
            ERROR_RESPONSE_MEMO_MALFORMED,
            ERROR_RESPONSE_MOVIE_MISSING,
            ERROR_RESPONSE_MOVIE_MALFORMED,
            ERROR_RESPONSE_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POST_DATE_MISSING,
            ERROR_RESPONSE_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POST_ID_MISSING,
            ERROR_RESPONSE_POST_ID_MALFORMED,
            ERROR_RESPONSE_REST_ID_MISSING,
            ERROR_RESPONSE_REST_ID_MALFORMED,
            ERROR_RESPONSE_RESTNAME_MISSING,
            ERROR_RESPONSE_RESTNAME_MALFORMED,
            ERROR_RESPONSE_LOCALITY_MISSING,
            ERROR_RESPONSE_LOCALITY_MALFORMED,
            ERROR_RESPONSE_THUMBNAIL_MISSING,
            ERROR_RESPONSE_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_VALUE_MISSING,
            ERROR_RESPONSE_VALUE_MALFORMED,
            ERROR_RESPONSE_USER_ID_MISSING,
            ERROR_RESPONSE_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERNAME_MISSING,
            ERROR_RESPONSE_USERNAME_MALFORMED,
            ERROR_RESPONSE_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetPostLocalCodeMessageTable(GetPostLocalCode code) {
            if (GetPostLocalCodeMap.isEmpty()) {
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_VALUE_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_PROFILE_IMG_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_REST_ID_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_POST_ID_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MOVIE_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_CATEGORY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_CATEGORY_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_POST_ID_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_POST_WAS_NEVER_COMPLETED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_POST_WAS_NEVER_COMPLETED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_CHEER_FLAG_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_REST_ID_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_CHEER_FLAG_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_RESTNAME_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_RESTNAME_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_USER_ID_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MEMO_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_CATEGORY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_CATEGORY_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_HLS_MOVIE_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_GOCHI_NUM_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_LOCALITY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_LOCALITY_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_POST_DATE_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_GOCHI_NUM_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_POST_DOES_NOT_EXIST, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_POST_DOES_NOT_EXIST));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_USERNAME_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_COMMENT_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_COMMENT_NUM_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_THUMBNAIL_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_THUMBNAIL_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MOVIE_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_COMMENT_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_COMMENT_NUM_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_HLS_MOVIE_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MEMO_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_VALUE_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_GOCHI_FLAG_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_POST_DATE_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_GOCHI_FLAG_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_PROFILE_IMG_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_LOCALITY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_LOCALITY_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MP4_MOVIE_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_USER_ID_MALFORMED));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_USERNAME_MISSING));
                GetPostLocalCodeMap.put(GetPostLocalCode.ERROR_RESPONSE_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetPostLocalCode_ERROR_RESPONSE_MP4_MOVIE_MISSING));
            }
            String message = null;
            for (Map.Entry<GetPostLocalCode, String> entry : GetPostLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetPostLocalCode GetPostLocalCodeReverseLookupTable(String message) {
            if (GetPostLocalCodeReverseMap.isEmpty()) {
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_VALUE_MISSING", GetPostLocalCode.ERROR_RESPONSE_VALUE_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MISSING", GetPostLocalCode.ERROR_RESPONSE_PROFILE_IMG_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_REST_ID_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_REST_ID_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_POST_ID_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MOVIE_MISSING", GetPostLocalCode.ERROR_RESPONSE_MOVIE_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_CATEGORY_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_CATEGORY_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_ID_MISSING", GetPostLocalCode.ERROR_RESPONSE_POST_ID_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetPostLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_POST_WAS_NEVER_COMPLETED", GetPostLocalCode.ERROR_POST_WAS_NEVER_COMPLETED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_CHEER_FLAG_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_CHEER_FLAG_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_REST_ID_MISSING", GetPostLocalCode.ERROR_RESPONSE_REST_ID_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_CHEER_FLAG_MISSING", GetPostLocalCode.ERROR_RESPONSE_CHEER_FLAG_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_RESTNAME_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_RESTNAME_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_RESTNAME_MISSING", GetPostLocalCode.ERROR_RESPONSE_RESTNAME_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_USER_ID_MISSING", GetPostLocalCode.ERROR_RESPONSE_USER_ID_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", GetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_MEMO_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_CATEGORY_MISSING", GetPostLocalCode.ERROR_RESPONSE_CATEGORY_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_HLS_MOVIE_MISSING", GetPostLocalCode.ERROR_RESPONSE_HLS_MOVIE_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_GOCHI_NUM_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_GOCHI_NUM_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_LOCALITY_MISSING", GetPostLocalCode.ERROR_RESPONSE_LOCALITY_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_DATE_MISSING", GetPostLocalCode.ERROR_RESPONSE_POST_DATE_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_GOCHI_NUM_MISSING", GetPostLocalCode.ERROR_RESPONSE_GOCHI_NUM_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_POST_DOES_NOT_EXIST", GetPostLocalCode.ERROR_POST_DOES_NOT_EXIST);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_USERNAME_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENT_NUM_MISSING", GetPostLocalCode.ERROR_RESPONSE_COMMENT_NUM_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_THUMBNAIL_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_THUMBNAIL_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_THUMBNAIL_MISSING", GetPostLocalCode.ERROR_RESPONSE_THUMBNAIL_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MOVIE_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_MOVIE_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENT_NUM_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_COMMENT_NUM_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_HLS_MOVIE_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_HLS_MOVIE_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_MISSING", GetPostLocalCode.ERROR_RESPONSE_MEMO_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_VALUE_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_VALUE_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_GOCHI_FLAG_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_GOCHI_FLAG_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_POST_DATE_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_POST_DATE_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_GOCHI_FLAG_MISSING", GetPostLocalCode.ERROR_RESPONSE_GOCHI_FLAG_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_PROFILE_IMG_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_PROFILE_IMG_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_LOCALITY_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_LOCALITY_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MP4_MOVIE_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_MP4_MOVIE_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_USER_ID_MALFORMED", GetPostLocalCode.ERROR_RESPONSE_USER_ID_MALFORMED);
                GetPostLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", GetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_USERNAME_MISSING", GetPostLocalCode.ERROR_RESPONSE_USERNAME_MISSING);
                GetPostLocalCodeReverseMap.put("ERROR_RESPONSE_MP4_MOVIE_MISSING", GetPostLocalCode.ERROR_RESPONSE_MP4_MOVIE_MISSING);
            }
            GetPostLocalCode code = null;
            for (Map.Entry<String, GetPostLocalCode> entry : GetPostLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetFollowlineLocalCode, String> GetFollowlineLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetFollowlineLocalCode> GetFollowlineLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetFollowlineAPI(String page, String category_id, String value_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/followline/");
            if (page != null) url.append("&page=").append(page);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value_id != null) url.append("&value_id=").append(value_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetFollowlineLocalCode {
            ERROR_PARAMETER_PAGE_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_RESTNAME_MISSING,
            ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_USER_ID_MISSING,
            ERROR_RESPONSE_POSTS_USER_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_USERNAME_MISSING,
            ERROR_RESPONSE_POSTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetFollowlineLocalCodeMessageTable(GetFollowlineLocalCode code) {
            if (GetFollowlineLocalCodeMap.isEmpty()) {
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_PARAMETER_PAGE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_PARAMETER_VALUE_ID_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_MISSING));
                GetFollowlineLocalCodeMap.put(GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowlineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING));
            }
            String message = null;
            for (Map.Entry<GetFollowlineLocalCode, String> entry : GetFollowlineLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetFollowlineLocalCode GetFollowlineLocalCodeReverseLookupTable(String message) {
            if (GetFollowlineLocalCodeReverseMap.isEmpty()) {
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", GetFollowlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_PARAMETER_PAGE_MALFORMED", GetFollowlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_ID_MALFORMED", GetFollowlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_MISSING);
                GetFollowlineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING", GetFollowlineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING);
            }
            GetFollowlineLocalCode code = null;
            for (Map.Entry<String, GetFollowlineLocalCode> entry : GetFollowlineLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetHeatmapLocalCode, String> GetHeatmapLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetHeatmapLocalCode> GetHeatmapLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetHeatmapAPI() {
            StringBuilder url = new StringBuilder(testurl + "/get/heatmap/");
            return url.toString().replace("/&", "/?");
        }

        public enum GetHeatmapLocalCode {
            ERROR_RESPONSE_RESTS_MISSING,
            ERROR_RESPONSE_RESTS_REST_ID_MISSING,
            ERROR_RESPONSE_RESTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_RESTS_RESTNAME_MISSING,
            ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_RESTS_LAT_MISSING,
            ERROR_RESPONSE_RESTS_LAT_MALFORMED,
            ERROR_RESPONSE_RESTS_LON_MISSING,
            ERROR_RESPONSE_RESTS_LON_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetHeatmapLocalCodeMessageTable(GetHeatmapLocalCode code) {
            if (GetHeatmapLocalCodeMap.isEmpty()) {
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MALFORMED));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MISSING));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_LON_MISSING));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_MISSING));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_LAT_MALFORMED));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_LAT_MISSING));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MISSING));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_RESTS_LON_MALFORMED));
                GetHeatmapLocalCodeMap.put(GetHeatmapLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetHeatmapLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<GetHeatmapLocalCode, String> entry : GetHeatmapLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetHeatmapLocalCode GetHeatmapLocalCodeReverseLookupTable(String message) {
            if (GetHeatmapLocalCodeReverseMap.isEmpty()) {
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MALFORMED", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LON_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LON_MISSING);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_MISSING);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LAT_MALFORMED", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LAT_MALFORMED);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LAT_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LAT_MISSING);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LON_MALFORMED", GetHeatmapLocalCode.ERROR_RESPONSE_RESTS_LON_MALFORMED);
                GetHeatmapLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetHeatmapLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            GetHeatmapLocalCode code = null;
            for (Map.Entry<String, GetHeatmapLocalCode> entry : GetHeatmapLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetFollower_RankLocalCode, String> GetFollower_RankLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetFollower_RankLocalCode> GetFollower_RankLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetFollowerRankAPI(String page) {
            StringBuilder url = new StringBuilder(testurl + "/get/follower_rank/");
            if (page != null) url.append("&page=").append(page);
            return url.toString().replace("/&", "/?");
        }

        public enum GetFollower_RankLocalCode {
            ERROR_PARAMETER_PAGE_MALFORMED,
            ERROR_RESPONSE_USERS_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_USERS_USER_ID_MISSING,
            ERROR_RESPONSE_USERS_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_USERNAME_MISSING,
            ERROR_RESPONSE_USERS_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetFollower_RankLocalCodeMessageTable(GetFollower_RankLocalCode code) {
            if (GetFollower_RankLocalCodeMap.isEmpty()) {
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_USERNAME_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_USERNAME_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_PARAMETER_PAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_PARAMETER_PAGE_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_USER_ID_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_USER_ID_MALFORMED));
                GetFollower_RankLocalCodeMap.put(GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollower_RankLocalCode_ERROR_RESPONSE_USERS_MISSING));
            }
            String message = null;
            for (Map.Entry<GetFollower_RankLocalCode, String> entry : GetFollower_RankLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetFollower_RankLocalCode GetFollower_RankLocalCodeReverseLookupTable(String message) {
            if (GetFollower_RankLocalCodeReverseMap.isEmpty()) {
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MALFORMED", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_PARAMETER_PAGE_MALFORMED", GetFollower_RankLocalCode.ERROR_PARAMETER_PAGE_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MALFORMED", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED);
                GetFollower_RankLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_MISSING", GetFollower_RankLocalCode.ERROR_RESPONSE_USERS_MISSING);
            }
            GetFollower_RankLocalCode code = null;
            for (Map.Entry<String, GetFollower_RankLocalCode> entry : GetFollower_RankLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetGochilineLocalCode, String> GetGochilineLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetGochilineLocalCode> GetGochilineLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetGochilineAPI(String page, String category_id, String value_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/gochiline/");
            if (page != null) url.append("&page=").append(page);
            if (category_id != null) url.append("&category_id=").append(category_id);
            if (value_id != null) url.append("&value_id=").append(value_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetGochilineLocalCode {
            ERROR_PARAMETER_PAGE_MALFORMED,
            ERROR_PARAMETER_CATEGORY_ID_MALFORMED,
            ERROR_PARAMETER_VALUE_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_RESTNAME_MISSING,
            ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_USER_ID_MISSING,
            ERROR_RESPONSE_POSTS_USER_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_USERNAME_MISSING,
            ERROR_RESPONSE_POSTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetGochilineLocalCodeMessageTable(GetGochilineLocalCode code) {
            if (GetGochilineLocalCodeMap.isEmpty()) {
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_PARAMETER_CATEGORY_ID_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_PARAMETER_PAGE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_PARAMETER_VALUE_ID_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_MISSING));
                GetGochilineLocalCodeMap.put(GetGochilineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetGochilineLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING));
            }
            String message = null;
            for (Map.Entry<GetGochilineLocalCode, String> entry : GetGochilineLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetGochilineLocalCode GetGochilineLocalCodeReverseLookupTable(String message) {
            if (GetGochilineLocalCodeReverseMap.isEmpty()) {
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_PARAMETER_CATEGORY_ID_MALFORMED", GetGochilineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_PARAMETER_PAGE_MALFORMED", GetGochilineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_PARAMETER_VALUE_ID_MALFORMED", GetGochilineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_MISSING);
                GetGochilineLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING", GetGochilineLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING);
            }
            GetGochilineLocalCode code = null;
            for (Map.Entry<String, GetGochilineLocalCode> entry : GetGochilineLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetFollowLocalCode, String> GetFollowLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetFollowLocalCode> GetFollowLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetFollowAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/follow/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetFollowLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_USERS_USER_ID_MISSING,
            ERROR_RESPONSE_USERS_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_USERNAME_MISSING,
            ERROR_RESPONSE_USERS_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetFollowLocalCodeMessageTable(GetFollowLocalCode code) {
            if (GetFollowLocalCodeMap.isEmpty()) {
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_USERNAME_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_USERNAME_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_USER_ID_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_USER_ID_MALFORMED));
                GetFollowLocalCodeMap.put(GetFollowLocalCode.ERROR_RESPONSE_USERS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowLocalCode_ERROR_RESPONSE_USERS_MISSING));
            }
            String message = null;
            for (Map.Entry<GetFollowLocalCode, String> entry : GetFollowLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetFollowLocalCode GetFollowLocalCodeReverseLookupTable(String message) {
            if (GetFollowLocalCodeReverseMap.isEmpty()) {
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED", GetFollowLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MALFORMED", GetFollowLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED", GetFollowLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED", GetFollowLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetFollowLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MALFORMED", GetFollowLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED);
                GetFollowLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_MISSING", GetFollowLocalCode.ERROR_RESPONSE_USERS_MISSING);
            }
            GetFollowLocalCode code = null;
            for (Map.Entry<String, GetFollowLocalCode> entry : GetFollowLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetUser_CheerLocalCode, String> GetUser_CheerLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetUser_CheerLocalCode> GetUser_CheerLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetUserCheerAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/user_cheer/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetUser_CheerLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_RESTS_MISSING,
            ERROR_RESPONSE_RESTS_LOCALITY_MISSING,
            ERROR_RESPONSE_RESTS_LOCALITY_MALFORMED,
            ERROR_RESPONSE_RESTS_REST_ID_MISSING,
            ERROR_RESPONSE_RESTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_RESTS_RESTNAME_MISSING,
            ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetUser_CheerLocalCodeMessageTable(GetUser_CheerLocalCode code) {
            if (GetUser_CheerLocalCodeMap.isEmpty()) {
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MALFORMED));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MISSING));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_LOCALITY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_LOCALITY_MISSING));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_MISSING));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_LOCALITY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_LOCALITY_MALFORMED));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_REST_ID_MISSING));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED));
                GetUser_CheerLocalCodeMap.put(GetUser_CheerLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUser_CheerLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
            }
            String message = null;
            for (Map.Entry<GetUser_CheerLocalCode, String> entry : GetUser_CheerLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetUser_CheerLocalCode GetUser_CheerLocalCodeReverseLookupTable(String message) {
            if (GetUser_CheerLocalCodeReverseMap.isEmpty()) {
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MALFORMED", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MALFORMED);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MISSING", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MISSING);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LOCALITY_MISSING", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_LOCALITY_MISSING);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_MISSING", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_MISSING);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_LOCALITY_MALFORMED", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_LOCALITY_MALFORMED);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_REST_ID_MISSING", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_REST_ID_MISSING);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED", GetUser_CheerLocalCode.ERROR_RESPONSE_RESTS_RESTNAME_MALFORMED);
                GetUser_CheerLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetUser_CheerLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
            }
            GetUser_CheerLocalCode code = null;
            for (Map.Entry<String, GetUser_CheerLocalCode> entry : GetUser_CheerLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetCommentLocalCode, String> GetCommentLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetCommentLocalCode> GetCommentLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetCommentAPI(String post_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/comment/");
            url.append("&post_id=").append(post_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetCommentLocalCode {
            ERROR_PARAMETER_POST_ID_MISSING,
            ERROR_PARAMETER_POST_ID_MALFORMED,
            ERROR_RESPONSE_MEMO_MISSING,
            ERROR_RESPONSE_MEMO_MEMO_MISSING,
            ERROR_RESPONSE_MEMO_MEMO_MALFORMED,
            ERROR_RESPONSE_MEMO_POST_DATE_MISSING,
            ERROR_RESPONSE_MEMO_POST_DATE_MALFORMED,
            ERROR_RESPONSE_MEMO_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_MEMO_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_MEMO_USER_ID_MISSING,
            ERROR_RESPONSE_MEMO_USER_ID_MALFORMED,
            ERROR_RESPONSE_MEMO_USERNAME_MISSING,
            ERROR_RESPONSE_MEMO_USERNAME_MALFORMED,
            ERROR_RESPONSE_COMMENTS_MISSING,
            ERROR_RESPONSE_COMMENTS_COMMENT_MISSING,
            ERROR_RESPONSE_COMMENTS_COMMENT_MALFORMED,
            ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MISSING,
            ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MALFORMED,
            ERROR_RESPONSE_COMMENTS_COMMENT_ID_MISSING,
            ERROR_RESPONSE_COMMENTS_COMMENT_ID_MALFORMED,
            ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MISSING,
            ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MALFORMED,
            ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_COMMENTS_RE_USERS_MISSING,
            ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MISSING,
            ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MALFORMED,
            ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MISSING,
            ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MALFORMED,
            ERROR_RESPONSE_COMMENTS_USERNAME_MISSING,
            ERROR_RESPONSE_COMMENTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetCommentLocalCodeMessageTable(GetCommentLocalCode code) {
            if (GetCommentLocalCodeMap.isEmpty()) {
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_MEMO_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_RE_USERS_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_USERNAME_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_POST_DATE_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_USER_ID_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_ID_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_USERNAME_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_ID_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_POST_DATE_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_USERNAME_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_MEMO_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_USERNAME_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_PROFILE_IMG_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_PROFILE_IMG_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_PARAMETER_POST_ID_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_MEMO_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_MEMO_USER_ID_MALFORMED));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_PARAMETER_POST_ID_MISSING));
                GetCommentLocalCodeMap.put(GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetCommentLocalCode_ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MISSING));
            }
            String message = null;
            for (Map.Entry<GetCommentLocalCode, String> entry : GetCommentLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetCommentLocalCode GetCommentLocalCodeReverseLookupTable(String message) {
            if (GetCommentLocalCodeReverseMap.isEmpty()) {
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_MEMO_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_MEMO_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_PROFILE_IMG_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_RE_USERS_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetCommentLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_USERNAME_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_USERNAME_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_POST_DATE_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_MEMO_POST_DATE_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_USER_ID_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_USER_ID_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_ID_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_ID_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_USERNAME_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_USERNAME_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_ID_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_ID_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_POST_DATE_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_POST_DATE_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_USERNAME_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_USERNAME_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_MEMO_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_MEMO_MEMO_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_USER_ID_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_USERNAME_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_MEMO_USERNAME_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_COMMENT_DATE_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_PROFILE_IMG_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_MEMO_PROFILE_IMG_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_PROFILE_IMG_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_PROFILE_IMG_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_MISSING", GetCommentLocalCode.ERROR_RESPONSE_MEMO_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USER_ID_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MALFORMED", GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_MEMO_USER_ID_MALFORMED", GetCommentLocalCode.ERROR_RESPONSE_MEMO_USER_ID_MALFORMED);
                GetCommentLocalCodeReverseMap.put("ERROR_PARAMETER_POST_ID_MISSING", GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING);
                GetCommentLocalCodeReverseMap.put("ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MISSING", GetCommentLocalCode.ERROR_RESPONSE_COMMENTS_RE_USERS_USERNAME_MISSING);
            }
            GetCommentLocalCode code = null;
            for (Map.Entry<String, GetCommentLocalCode> entry : GetCommentLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetFollowerLocalCode, String> GetFollowerLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetFollowerLocalCode> GetFollowerLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetFollowerAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/follower/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetFollowerLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_USERS_USER_ID_MISSING,
            ERROR_RESPONSE_USERS_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_USERNAME_MISSING,
            ERROR_RESPONSE_USERS_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetFollowerLocalCodeMessageTable(GetFollowerLocalCode code) {
            if (GetFollowerLocalCodeMap.isEmpty()) {
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_USERNAME_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_USERNAME_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_USER_ID_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_USER_ID_MALFORMED));
                GetFollowerLocalCodeMap.put(GetFollowerLocalCode.ERROR_RESPONSE_USERS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetFollowerLocalCode_ERROR_RESPONSE_USERS_MISSING));
            }
            String message = null;
            for (Map.Entry<GetFollowerLocalCode, String> entry : GetFollowerLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetFollowerLocalCode GetFollowerLocalCodeReverseLookupTable(String message) {
            if (GetFollowerLocalCodeReverseMap.isEmpty()) {
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED", GetFollowerLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MALFORMED", GetFollowerLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED", GetFollowerLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED", GetFollowerLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MALFORMED", GetFollowerLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED);
                GetFollowerLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_MISSING", GetFollowerLocalCode.ERROR_RESPONSE_USERS_MISSING);
            }
            GetFollowerLocalCode code = null;
            for (Map.Entry<String, GetFollowerLocalCode> entry : GetFollowerLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetUserLocalCode, String> GetUserLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetUserLocalCode> GetUserLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetUserAPI(String user_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/user/");
            url.append("&user_id=").append(user_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetUserLocalCode {
            ERROR_PARAMETER_USER_ID_MISSING,
            ERROR_PARAMETER_USER_ID_MALFORMED,
            ERROR_RESPONSE_USER_MISSING,
            ERROR_RESPONSE_USER_CHEER_NUM_MISSING,
            ERROR_RESPONSE_USER_CHEER_NUM_MALFORMED,
            ERROR_RESPONSE_USER_FOLLOW_FLAG_MISSING,
            ERROR_RESPONSE_USER_FOLLOW_FLAG_MALFORMED,
            ERROR_RESPONSE_USER_FOLLOW_NUM_MISSING,
            ERROR_RESPONSE_USER_FOLLOW_NUM_MALFORMED,
            ERROR_RESPONSE_USER_FOLLOWER_NUM_MISSING,
            ERROR_RESPONSE_USER_FOLLOWER_NUM_MALFORMED,
            ERROR_RESPONSE_USER_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_USER_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_USER_USER_ID_MISSING,
            ERROR_RESPONSE_USER_USER_ID_MALFORMED,
            ERROR_RESPONSE_USER_USERNAME_MISSING,
            ERROR_RESPONSE_USER_USERNAME_MALFORMED,
            ERROR_RESPONSE_USER_POST_NUM_MISSING,
            ERROR_RESPONSE_USER_POST_NUM_MALFORMED,
            ERROR_RESPONSE_USER_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_USER_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CATEGORY_MISSING,
            ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING,
            ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_LAT_MISSING,
            ERROR_RESPONSE_POSTS_LAT_MALFORMED,
            ERROR_RESPONSE_POSTS_LON_MISSING,
            ERROR_RESPONSE_POSTS_LON_MALFORMED,
            ERROR_RESPONSE_POSTS_MEMO_MISSING,
            ERROR_RESPONSE_POSTS_MEMO_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_RESTNAME_MISSING,
            ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetUserLocalCodeMessageTable(GetUserLocalCode code) {
            if (GetUserLocalCodeMap.isEmpty()) {
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_GOCHI_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_CHEER_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_CHEER_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_USERNAME_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOW_FLAG_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOWER_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOWER_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_CHEER_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_CHEER_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_PARAMETER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_PARAMETER_USER_ID_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_POST_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_POST_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_PARAMETER_USER_ID_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MEMO_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_LAT_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_LON_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_LON_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_USER_ID_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOW_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOW_FLAG_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_USERNAME_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_LAT_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOW_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_USER_ID_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_POST_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_POST_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_RESTNAME_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MEMO_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_REST_ID_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_GOCHI_NUM_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_CATEGORY_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_PROFILE_IMG_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOWER_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_FOLLOWER_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_POSTS_MISSING));
                GetUserLocalCodeMap.put(GetUserLocalCode.ERROR_RESPONSE_USER_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUserLocalCode_ERROR_RESPONSE_USER_PROFILE_IMG_MALFORMED));
            }
            String message = null;
            for (Map.Entry<GetUserLocalCode, String> entry : GetUserLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetUserLocalCode GetUserLocalCodeReverseLookupTable(String message) {
            if (GetUserLocalCodeReverseMap.isEmpty()) {
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_GOCHI_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_GOCHI_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_CHEER_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_CHEER_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_USERNAME_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_USERNAME_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOW_FLAG_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_FLAG_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOWER_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOWER_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_CHEER_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_CHEER_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MISSING", GetUserLocalCode.ERROR_PARAMETER_USER_ID_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_POST_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_POST_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_PARAMETER_USER_ID_MALFORMED", GetUserLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MEMO_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_MEMO_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LAT_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_LAT_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LON_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_LON_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LON_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_LON_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_USER_ID_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_USER_ID_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOW_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOW_FLAG_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_FLAG_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_USERNAME_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_USERNAME_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_LAT_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_LAT_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOW_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOW_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_USER_ID_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_USER_ID_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_POST_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_POST_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_RESTNAME_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_RESTNAME_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MEMO_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_MEMO_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_REST_ID_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_REST_ID_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_GOCHI_NUM_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_GOCHI_NUM_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CATEGORY_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_PROFILE_IMG_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_PROFILE_IMG_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_FOLLOWER_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_USER_FOLLOWER_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetUserLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetUserLocalCode.ERROR_RESPONSE_POSTS_MISSING);
                GetUserLocalCodeReverseMap.put("ERROR_RESPONSE_USER_PROFILE_IMG_MALFORMED", GetUserLocalCode.ERROR_RESPONSE_USER_PROFILE_IMG_MALFORMED);
            }
            GetUserLocalCode code = null;
            for (Map.Entry<String, GetUserLocalCode> entry : GetUserLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetRestLocalCode, String> GetRestLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetRestLocalCode> GetRestLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetRestAPI(String rest_id) {
            StringBuilder url = new StringBuilder(testurl + "/get/rest/");
            url.append("&rest_id=").append(rest_id);
            return url.toString().replace("/&", "/?");
        }

        public enum GetRestLocalCode {
            ERROR_PARAMETER_REST_ID_MISSING,
            ERROR_PARAMETER_REST_ID_MALFORMED,
            ERROR_RESPONSE_REST_MISSING,
            ERROR_RESPONSE_REST_HOMEPAGE_MISSING,
            ERROR_RESPONSE_REST_HOMEPAGE_MALFORMED,
            ERROR_RESPONSE_REST_LAT_MISSING,
            ERROR_RESPONSE_REST_LAT_MALFORMED,
            ERROR_RESPONSE_REST_LON_MISSING,
            ERROR_RESPONSE_REST_LON_MALFORMED,
            ERROR_RESPONSE_REST_LOCALITY_MISSING,
            ERROR_RESPONSE_REST_LOCALITY_MALFORMED,
            ERROR_RESPONSE_REST_REST_CATEGORY_MISSING,
            ERROR_RESPONSE_REST_REST_CATEGORY_MALFORMED,
            ERROR_RESPONSE_REST_REST_ID_MISSING,
            ERROR_RESPONSE_REST_REST_ID_MALFORMED,
            ERROR_RESPONSE_REST_RESTNAME_MISSING,
            ERROR_RESPONSE_REST_RESTNAME_MALFORMED,
            ERROR_RESPONSE_REST_TELL_MISSING,
            ERROR_RESPONSE_REST_TELL_MALFORMED,
            ERROR_RESPONSE_POSTS_MISSING,
            ERROR_RESPONSE_POSTS_CATEGORY_MISSING,
            ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING,
            ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED,
            ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MEMO_MISSING,
            ERROR_RESPONSE_POSTS_MEMO_MALFORMED,
            ERROR_RESPONSE_POSTS_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING,
            ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_DATE_MISSING,
            ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_POST_REST_ID_MISSING,
            ERROR_RESPONSE_POSTS_POST_REST_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING,
            ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED,
            ERROR_RESPONSE_POSTS_USER_ID_MISSING,
            ERROR_RESPONSE_POSTS_USER_ID_MALFORMED,
            ERROR_RESPONSE_POSTS_USERNAME_MISSING,
            ERROR_RESPONSE_POSTS_USERNAME_MALFORMED,
            ERROR_RESPONSE_POSTS_VALUE_MISSING,
            ERROR_RESPONSE_POSTS_VALUE_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetRestLocalCodeMessageTable(GetRestLocalCode code) {
            if (GetRestLocalCodeMap.isEmpty()) {
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LOCALITY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LOCALITY_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_PARAMETER_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_PARAMETER_REST_ID_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_VALUE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LON_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LON_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_HOMEPAGE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_HOMEPAGE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_RESTNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_RESTNAME_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_TELL_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_TELL_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MEMO_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MEMO_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MOVIE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_REST_ID_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LAT_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LAT_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LAT_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LAT_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_PARAMETER_REST_ID_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_REST_ID_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_REST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_REST_ID_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_RESTNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_RESTNAME_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_REST_CATEGORY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_REST_CATEGORY_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_HOMEPAGE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_HOMEPAGE_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_REST_CATEGORY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_REST_CATEGORY_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MEMO_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MEMO_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_REST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_REST_ID_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LON_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LON_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_LOCALITY_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_LOCALITY_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_VALUE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_USER_ID_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_REST_TELL_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_REST_TELL_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_CATEGORY_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_ID_MALFORMED));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_POST_DATE_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_USERNAME_MISSING));
                GetRestLocalCodeMap.put(GetRestLocalCode.ERROR_RESPONSE_POSTS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetRestLocalCode_ERROR_RESPONSE_POSTS_MISSING));
            }
            String message = null;
            for (Map.Entry<GetRestLocalCode, String> entry : GetRestLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetRestLocalCode GetRestLocalCodeReverseLookupTable(String message) {
            if (GetRestLocalCodeReverseMap.isEmpty()) {
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LOCALITY_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_LOCALITY_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_PARAMETER_REST_ID_MISSING", GetRestLocalCode.ERROR_PARAMETER_REST_ID_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_VALUE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LON_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_LON_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_HOMEPAGE_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_HOMEPAGE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_NUM_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_RESTNAME_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_RESTNAME_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_TELL_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_TELL_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MEMO_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_MEMO_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MOVIE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_MOVIE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_REST_ID_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_REST_ID_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LAT_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_LAT_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_MP4_MOVIE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_PROFILE_IMG_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LAT_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_LAT_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_THUMBNAIL_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_PARAMETER_REST_ID_MALFORMED", GetRestLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_REST_ID_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_REST_ID_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_REST_ID_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_REST_ID_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetRestLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_RESTNAME_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_RESTNAME_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_HLS_MOVIE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_REST_CATEGORY_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_REST_CATEGORY_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_HOMEPAGE_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_HOMEPAGE_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_REST_CATEGORY_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_REST_CATEGORY_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MEMO_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_MEMO_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_CHEER_FLAG_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_REST_ID_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_REST_ID_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LON_MISSING", GetRestLocalCode.ERROR_RESPONSE_REST_LON_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_LOCALITY_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_LOCALITY_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_VALUE_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_VALUE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USER_ID_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_USER_ID_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_REST_TELL_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_REST_TELL_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_CATEGORY_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_CATEGORY_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_ID_MALFORMED", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_ID_MALFORMED);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_POST_DATE_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_POST_DATE_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_COMMENT_NUM_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_GOCHI_FLAG_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_USERNAME_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_USERNAME_MISSING);
                GetRestLocalCodeReverseMap.put("ERROR_RESPONSE_POSTS_MISSING", GetRestLocalCode.ERROR_RESPONSE_POSTS_MISSING);
            }
            GetRestLocalCode code = null;
            for (Map.Entry<String, GetRestLocalCode> entry : GetRestLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetNoticeLocalCode, String> GetNoticeLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetNoticeLocalCode> GetNoticeLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetNoticeAPI() {
            StringBuilder url = new StringBuilder(testurl + "/get/notice/");
            return url.toString().replace("/&", "/?");
        }

        public enum GetNoticeLocalCode {
            ERROR_RESPONSE_NOTICES_MISSING,
            ERROR_RESPONSE_NOTICES_NOTICE_MISSING,
            ERROR_RESPONSE_NOTICES_NOTICE_MALFORMED,
            ERROR_RESPONSE_NOTICES_NOTICE_DATE_MISSING,
            ERROR_RESPONSE_NOTICES_NOTICE_DATE_MALFORMED,
            ERROR_RESPONSE_NOTICES_NOTICE_ID_MISSING,
            ERROR_RESPONSE_NOTICES_NOTICE_ID_MALFORMED,
            ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MISSING,
            ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MALFORMED,
            ERROR_RESPONSE_NOTICES_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_NOTICES_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_NOTICES_USER_ID_MISSING,
            ERROR_RESPONSE_NOTICES_USER_ID_MALFORMED,
            ERROR_RESPONSE_NOTICES_USERNAME_MISSING,
            ERROR_RESPONSE_NOTICES_USERNAME_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetNoticeLocalCodeMessageTable(GetNoticeLocalCode code) {
            if (GetNoticeLocalCodeMap.isEmpty()) {
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_DATE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_DATE_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_USERNAME_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_USER_ID_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_USER_ID_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_USERNAME_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_DATE_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_DATE_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_PROFILE_IMG_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_PROFILE_IMG_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MALFORMED));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_ID_MISSING));
                GetNoticeLocalCodeMap.put(GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetNoticeLocalCode_ERROR_RESPONSE_NOTICES_NOTICE_ID_MALFORMED));
            }
            String message = null;
            for (Map.Entry<GetNoticeLocalCode, String> entry : GetNoticeLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetNoticeLocalCode GetNoticeLocalCodeReverseLookupTable(String message) {
            if (GetNoticeLocalCodeReverseMap.isEmpty()) {
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_DATE_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_DATE_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_USERNAME_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USERNAME_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_USER_ID_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USER_ID_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_USER_ID_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USER_ID_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_USERNAME_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_USERNAME_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_DATE_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_DATE_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_PROFILE_IMG_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_PROFILE_IMG_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_PROFILE_IMG_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_PROFILE_IMG_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_POST_ID_MALFORMED);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_ID_MISSING", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_ID_MISSING);
                GetNoticeLocalCodeReverseMap.put("ERROR_RESPONSE_NOTICES_NOTICE_ID_MALFORMED", GetNoticeLocalCode.ERROR_RESPONSE_NOTICES_NOTICE_ID_MALFORMED);
            }
            GetNoticeLocalCode code = null;
            for (Map.Entry<String, GetNoticeLocalCode> entry : GetNoticeLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

        private static final ConcurrentHashMap<GetUsernameLocalCode, String> GetUsernameLocalCodeMap = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, GetUsernameLocalCode> GetUsernameLocalCodeReverseMap = new ConcurrentHashMap<>();

        public static String getGetUsernameAPI(String username) {
            StringBuilder url = new StringBuilder(testurl + "/get/username/");
            url.append("&username=").append(username);
            return url.toString().replace("/&", "/?");
        }

        public enum GetUsernameLocalCode {
            ERROR_PARAMETER_USERNAME_MISSING,
            ERROR_PARAMETER_USERNAME_MALFORMED,
            ERROR_RESPONSE_USERS_MISSING,
            ERROR_RESPONSE_USERS_USER_ID_MISSING,
            ERROR_RESPONSE_USERS_USER_ID_MALFORMED,
            ERROR_RESPONSE_USERS_USERNAME_MISSING,
            ERROR_RESPONSE_USERS_USERNAME_MALFORMED,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING,
            ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING,
            ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING,
            ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED,
            ERROR_RESPONSE_PAYLOAD_MISSING,
        }

        public static String GetUsernameLocalCodeMessageTable(GetUsernameLocalCode code) {
            if (GetUsernameLocalCodeMap.isEmpty()) {
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_PARAMETER_USERNAME_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_USERNAME_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_USERNAME_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_PAYLOAD_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_USER_ID_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_PARAMETER_USERNAME_MISSING));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_USER_ID_MALFORMED));
                GetUsernameLocalCodeMap.put(GetUsernameLocalCode.ERROR_RESPONSE_USERS_MISSING, Application_Gocci.getInstance().getApplicationContext().getString(R.string.GetUsernameLocalCode_ERROR_RESPONSE_USERS_MISSING));
            }
            String message = null;
            for (Map.Entry<GetUsernameLocalCode, String> entry : GetUsernameLocalCodeMap.entrySet()) {
                if (entry.getKey().equals(code)) {
                    message = entry.getValue();
                    break;
                }
            }
            return message;
        }

        public static GetUsernameLocalCode GetUsernameLocalCodeReverseLookupTable(String message) {
            if (GetUsernameLocalCodeReverseMap.isEmpty()) {
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED", GetUsernameLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MALFORMED", GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_USERNAME_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USERNAME_MALFORMED", GetUsernameLocalCode.ERROR_RESPONSE_USERS_USERNAME_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED", GetUsernameLocalCode.ERROR_RESPONSE_USERS_FOLLOW_FLAG_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED", GetUsernameLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_GOCHI_NUM_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_PAYLOAD_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_PAYLOAD_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_USER_ID_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_PROFILE_IMG_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_PARAMETER_USERNAME_MISSING", GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_USER_ID_MALFORMED", GetUsernameLocalCode.ERROR_RESPONSE_USERS_USER_ID_MALFORMED);
                GetUsernameLocalCodeReverseMap.put("ERROR_RESPONSE_USERS_MISSING", GetUsernameLocalCode.ERROR_RESPONSE_USERS_MISSING);
            }
            GetUsernameLocalCode code = null;
            for (Map.Entry<String, GetUsernameLocalCode> entry : GetUsernameLocalCodeReverseMap.entrySet()) {
                if (entry.getKey().equals(message)) {
                    code = entry.getValue();
                    break;
                }
            }
            return code;
        }

    }

    class Impl implements API3 {
        private static Impl sAPI3;

        public Impl() {
        }

        public static Impl getRepository() {
            if (sAPI3 == null) {
                sAPI3 = new Impl();
            }
            return sAPI3;
        }

        @Override
        public Util.UnsetSns_LinkLocalCode UnsetSns_LinkParameterRegex(String provider, String sns_token) {
            if (provider != null) {
                if (!provider.matches("^(api.twitter.com)|(graph.facebook.com)$")) {
                    return Util.UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED;
                }
            } else {
                return Util.UnsetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING;
            }
            if (sns_token != null) {
                if (!sns_token.matches("^[^\\p{Cntrl}]{20,4000}$")) {
                    return Util.UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED;
                }
            } else {
                return Util.UnsetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING;
            }
            return null;
        }

        @Override
        public void UnsetSns_LinkResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetSns_LinkLocalCode localCode = Util.UnsetSns_LinkLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetSns_LinkLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.UnsetDeviceLocalCode UnsetDeviceParameterRegex() {
            return null;
        }

        @Override
        public void UnsetDeviceResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetDeviceLocalCode localCode = Util.UnsetDeviceLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetDeviceLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.UnsetFollowLocalCode UnsetFollowParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.UnsetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void UnsetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetFollowLocalCode localCode = Util.UnsetFollowLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetFollowLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.UnsetCommentLocalCode UnsetCommentParameterRegex(String comment_id) {
            if (comment_id != null) {
                if (!comment_id.matches("^\\d{1,9}$")) {
                    return Util.UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED;
                }
            } else {
                return Util.UnsetCommentLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING;
            }
            return null;
        }

        @Override
        public void UnsetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetCommentLocalCode localCode = Util.UnsetCommentLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetCommentLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.UnsetGochiLocalCode UnsetGochiParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.UnsetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void UnsetGochiResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetGochiLocalCode localCode = Util.UnsetGochiLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetGochiLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.UnsetPostLocalCode UnsetPostParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.UnsetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void UnsetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.UnsetPostLocalCode localCode = Util.UnsetPostLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.UnsetPostLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetSns_LinkLocalCode SetSns_LinkParameterRegex(String provider, String sns_token) {
            if (provider != null) {
                if (!provider.matches("^(api.twitter.com)|(graph.facebook.com)$")) {
                    return Util.SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MALFORMED;
                }
            } else {
                return Util.SetSns_LinkLocalCode.ERROR_PARAMETER_PROVIDER_MISSING;
            }
            if (sns_token != null) {
                if (!sns_token.matches("^[^\\p{Cntrl}]{20,4000}$")) {
                    return Util.SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MALFORMED;
                }
            } else {
                return Util.SetSns_LinkLocalCode.ERROR_PARAMETER_SNS_TOKEN_MISSING;
            }
            return null;
        }

        @Override
        public void SetSns_LinkResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetSns_LinkLocalCode localCode = Util.SetSns_LinkLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetSns_LinkLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetDeviceLocalCode SetDeviceParameterRegex(String device_token, String os, String ver, String model) {
            if (device_token != null) {
                if (!device_token.matches("^([a-f0-9]{64})|([a-zA-Z0-9:_-]{140,250})$")) {
                    return Util.SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MALFORMED;
                }
            } else {
                return Util.SetDeviceLocalCode.ERROR_PARAMETER_DEVICE_TOKEN_MISSING;
            }
            if (os != null) {
                if (!os.matches("^android$|^iOS$")) {
                    return Util.SetDeviceLocalCode.ERROR_PARAMETER_OS_MALFORMED;
                }
            } else {
                return Util.SetDeviceLocalCode.ERROR_PARAMETER_OS_MISSING;
            }
            if (ver != null) {
                if (!ver.matches("^[0-9.]{1,6}$")) {
                    return Util.SetDeviceLocalCode.ERROR_PARAMETER_VER_MALFORMED;
                }
            } else {
                return Util.SetDeviceLocalCode.ERROR_PARAMETER_VER_MISSING;
            }
            if (model != null) {
                if (!model.matches("^[^\\p{Cntrl}]{1,50}$")) {
                    return Util.SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MALFORMED;
                }
            } else {
                return Util.SetDeviceLocalCode.ERROR_PARAMETER_MODEL_MISSING;
            }
            return null;
        }

        @Override
        public void SetDeviceResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetDeviceLocalCode localCode = Util.SetDeviceLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetDeviceLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetPostLocalCode SetPostParameterRegex(String rest_id, String movie_name, String category_id, String value, String memo, String cheer_flag) {
            if (rest_id != null) {
                if (!rest_id.matches("^\\d{1,9}$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED;
                }
            } else {
                return Util.SetPostLocalCode.ERROR_PARAMETER_REST_ID_MISSING;
            }
            if (movie_name != null) {
                if (!movie_name.matches("^\\d{4}(-\\d{2}){5}_\\d{1,9}$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED;
                }
            } else {
                return Util.SetPostLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING;
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value != null) {
                if (!value.matches("^\\d{1,9}$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_VALUE_MALFORMED;
                }
            }
            if (memo != null) {
                if (!memo.matches("^(\\n|[^\\p{Cntrl}]){1,1000}$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_MEMO_MALFORMED;
                }
            }
            if (cheer_flag != null) {
                if (!cheer_flag.matches("^1|0$")) {
                    return Util.SetPostLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void SetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetPostLocalCode localCode = Util.SetPostLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetPostLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetCommentLocalCode SetCommentParameterRegex(String post_id, String comment, String re_user_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.SetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            if (comment != null) {
                if (!comment.matches("^(\\n|[^\\p{Cntrl}]){1,140}$")) {
                    return Util.SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED;
                }
            } else {
                return Util.SetCommentLocalCode.ERROR_PARAMETER_COMMENT_MISSING;
            }
            if (re_user_id != null) {
                if (!re_user_id.matches("^[0-9,]{1,9}$")) {
                    return Util.SetCommentLocalCode.ERROR_PARAMETER_RE_USER_ID_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void SetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetCommentLocalCode localCode = Util.SetCommentLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetCommentLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetComment_BlockLocalCode SetComment_BlockParameterRegex(String comment_id) {
            if (comment_id != null) {
                if (!comment_id.matches("^\\d{1,9}$")) {
                    return Util.SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED;
                }
            } else {
                return Util.SetComment_BlockLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING;
            }
            return null;
        }

        @Override
        public void SetComment_BlockResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetComment_BlockLocalCode localCode = Util.SetComment_BlockLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetComment_BlockLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetRestLocalCode SetRestParameterRegex(String restname, String lat, String lon) {
            if (restname != null) {
                if (!restname.matches("^[^\\p{Cntrl}]{1,80}$")) {
                    return Util.SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED;
                }
            } else {
                return Util.SetRestLocalCode.ERROR_PARAMETER_RESTNAME_MISSING;
            }
            if (lat != null) {
                if (!lat.matches("^-?\\d{1,3}.\\d{1,20}$")) {
                    return Util.SetRestLocalCode.ERROR_PARAMETER_LAT_MALFORMED;
                }
            } else {
                return Util.SetRestLocalCode.ERROR_PARAMETER_LAT_MISSING;
            }
            if (lon != null) {
                if (!lon.matches("^-?\\d{1,3}.\\d{1,20}$")) {
                    return Util.SetRestLocalCode.ERROR_PARAMETER_LON_MALFORMED;
                }
            } else {
                return Util.SetRestLocalCode.ERROR_PARAMETER_LON_MISSING;
            }
            return null;
        }

        @Override
        public void SetRestResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetRestLocalCode localCode = Util.SetRestLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetRestLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetPost_CrashLocalCode SetPost_CrashParameterRegex(String restname, String address, String movie_name, String category_id, String value, String memo, String cheer_flag) {
            if (restname != null) {
                if (!restname.matches("^[^\\p{Cntrl}]{1,80}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MALFORMED;
                }
            } else {
                return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_RESTNAME_MISSING;
            }
            if (address != null) {
                if (!address.matches("^[^\\p{Cntrl}]{1,100}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MALFORMED;
                }
            } else {
                return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_ADDRESS_MISSING;
            }
            if (movie_name != null) {
                if (!movie_name.matches("^\\d{4}(-\\d{2}){5}_\\d{1,9}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MALFORMED;
                }
            } else {
                return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_MOVIE_NAME_MISSING;
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value != null) {
                if (!value.matches("^\\d{1,9}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_VALUE_MALFORMED;
                }
            }
            if (memo != null) {
                if (!memo.matches("^(\\n|[^\\p{Cntrl}]){1,1000}$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_MEMO_MALFORMED;
                }
            }
            if (cheer_flag != null) {
                if (!cheer_flag.matches("^1|0$")) {
                    return Util.SetPost_CrashLocalCode.ERROR_PARAMETER_CHEER_FLAG_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void SetPost_CrashResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetPost_CrashLocalCode localCode = Util.SetPost_CrashLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetPost_CrashLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetGochiLocalCode SetGochiParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.SetGochiLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void SetGochiResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetGochiLocalCode localCode = Util.SetGochiLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetGochiLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetProfile_ImgLocalCode SetProfile_ImgParameterRegex(String profile_img) {
            if (profile_img != null) {
                if (!profile_img.matches("^[0-9_-]+_img$")) {
                    return Util.SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MALFORMED;
                }
            } else {
                return Util.SetProfile_ImgLocalCode.ERROR_PARAMETER_PROFILE_IMG_MISSING;
            }
            return null;
        }

        @Override
        public void SetProfile_ImgResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetProfile_ImgLocalCode localCode = Util.SetProfile_ImgLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetProfile_ImgLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetPasswordLocalCode SetPasswordParameterRegex(String password) {
            if (password != null) {
                if (!password.matches("^[^\\p{Cntrl}]{6,25}$")) {
                    return Util.SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED;
                }
            } else {
                return Util.SetPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING;
            }
            return null;
        }

        @Override
        public void SetPasswordResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetPasswordLocalCode localCode = Util.SetPasswordLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetPasswordLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetComment_EditLocalCode SetComment_EditParameterRegex(String comment_id, String comment) {
            if (comment_id != null) {
                if (!comment_id.matches("^\\d{1,9}$")) {
                    return Util.SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MALFORMED;
                }
            } else {
                return Util.SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_ID_MISSING;
            }
            if (comment != null) {
                if (!comment.matches("^(\\n|[^\\p{Cntrl}]){1,140}$")) {
                    return Util.SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MALFORMED;
                }
            } else {
                return Util.SetComment_EditLocalCode.ERROR_PARAMETER_COMMENT_MISSING;
            }
            return null;
        }

        @Override
        public void SetComment_EditResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetComment_EditLocalCode localCode = Util.SetComment_EditLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetComment_EditLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetFeedbackLocalCode SetFeedbackParameterRegex(String feedback) {
            if (feedback != null) {
                if (!feedback.matches("^[^\\p{Cntrl}]{1,10000}$")) {
                    return Util.SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MALFORMED;
                }
            } else {
                return Util.SetFeedbackLocalCode.ERROR_PARAMETER_FEEDBACK_MISSING;
            }
            return null;
        }

        @Override
        public void SetFeedbackResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetFeedbackLocalCode localCode = Util.SetFeedbackLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetFeedbackLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetFollowLocalCode SetFollowParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.SetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void SetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetFollowLocalCode localCode = Util.SetFollowLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetFollowLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetMemo_EditLocalCode SetMemo_EditParameterRegex(String post_id, String memo) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.SetMemo_EditLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            if (memo != null) {
                if (!memo.matches("^(\\n|[^\\p{Cntrl}]){1,1000}$")) {
                    return Util.SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MALFORMED;
                }
            } else {
                return Util.SetMemo_EditLocalCode.ERROR_PARAMETER_MEMO_MISSING;
            }
            return null;
        }

        @Override
        public void SetMemo_EditResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetMemo_EditLocalCode localCode = Util.SetMemo_EditLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetMemo_EditLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetUsernameLocalCode SetUsernameParameterRegex(String username) {
            if (username != null) {
                if (!username.matches("^[^\\p{Cntrl}]{1,20}$")) {
                    return Util.SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED;
                }
            } else {
                return Util.SetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING;
            }
            return null;
        }

        @Override
        public void SetUsernameResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetUsernameLocalCode localCode = Util.SetUsernameLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetUsernameLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.SetPost_BlockLocalCode SetPost_BlockParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.SetPost_BlockLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void SetPost_BlockResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.SetPost_BlockLocalCode localCode = Util.SetPost_BlockLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.SetPost_BlockLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.AuthLoginLocalCode AuthLoginParameterRegex(String identity_id) {
            if (identity_id != null) {
                if (!identity_id.matches("^us-east-1:[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}$")) {
                    return Util.AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MALFORMED;
                }
            } else {
                return Util.AuthLoginLocalCode.ERROR_PARAMETER_IDENTITY_ID_MISSING;
            }
            return null;
        }

        @Override
        public void AuthLoginResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.AuthLoginLocalCode localCode = Util.AuthLoginLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.AuthLoginLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.AuthSignupLocalCode AuthSignupParameterRegex(String username) {
            if (username != null) {
                if (!username.matches("^[^\\p{Cntrl}]{1,20}$")) {
                    return Util.AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED;
                }
            } else {
                return Util.AuthSignupLocalCode.ERROR_PARAMETER_USERNAME_MISSING;
            }
            return null;
        }

        @Override
        public void AuthSignupResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.AuthSignupLocalCode localCode = Util.AuthSignupLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.AuthSignupLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.AuthPasswordLocalCode AuthPasswordParameterRegex(String username, String password) {
            if (username != null) {
                if (!username.matches("^[^\\p{Cntrl}]{1,20}$")) {
                    return Util.AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED;
                }
            } else {
                return Util.AuthPasswordLocalCode.ERROR_PARAMETER_USERNAME_MISSING;
            }
            if (password != null) {
                if (!password.matches("^[^\\p{Cntrl}]{6,25}$")) {
                    return Util.AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MALFORMED;
                }
            } else {
                return Util.AuthPasswordLocalCode.ERROR_PARAMETER_PASSWORD_MISSING;
            }
            return null;
        }

        @Override
        public void AuthPasswordResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.AuthPasswordLocalCode localCode = Util.AuthPasswordLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.AuthPasswordLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetTimelineLocalCode GetTimelineParameterRegex(String page, String category_id, String value_id) {
            if (page != null) {
                if (!page.matches("^\\d{1,9}$")) {
                    return Util.GetTimelineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED;
                }
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.GetTimelineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value_id != null) {
                if (!value_id.matches("^\\d{1,9}$")) {
                    return Util.GetTimelineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void GetTimelineResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetTimelineLocalCode localCode = Util.GetTimelineLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetTimelineLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetNearLocalCode GetNearParameterRegex(String lat, String lon) {
            if (lat != null) {
                if (!lat.matches("^-?\\d{1,3}.\\d{1,20}$")) {
                    return Util.GetNearLocalCode.ERROR_PARAMETER_LAT_MALFORMED;
                }
            } else {
                return Util.GetNearLocalCode.ERROR_PARAMETER_LAT_MISSING;
            }
            if (lon != null) {
                if (!lon.matches("^-?\\d{1,3}.\\d{1,20}$")) {
                    return Util.GetNearLocalCode.ERROR_PARAMETER_LON_MALFORMED;
                }
            } else {
                return Util.GetNearLocalCode.ERROR_PARAMETER_LON_MISSING;
            }
            return null;
        }

        @Override
        public void GetNearResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetNearLocalCode localCode = Util.GetNearLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetNearLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetNearlineLocalCode GetNearlineParameterRegex(String lat, String lon, String page, String category_id, String value_id) {
            if (lat != null) {
                if (!lat.matches("^-?\\d{1,3}\\.\\d{1,20}$")) {
                    return Util.GetNearlineLocalCode.ERROR_PARAMETER_LAT_MALFORMED;
                }
            } else {
                return Util.GetNearlineLocalCode.ERROR_PARAMETER_LAT_MISSING;
            }
            if (lon != null) {
                if (!lon.matches("^-?\\d{1,3}\\.\\d{1,20}$")) {
                    return Util.GetNearlineLocalCode.ERROR_PARAMETER_LON_MALFORMED;
                }
            } else {
                return Util.GetNearlineLocalCode.ERROR_PARAMETER_LON_MISSING;
            }
            if (page != null) {
                if (!page.matches("^\\d{1,9}$")) {
                    return Util.GetNearlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED;
                }
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.GetNearlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value_id != null) {
                if (!value_id.matches("^\\d{1,9}$")) {
                    return Util.GetNearlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void GetNearlineResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetNearlineLocalCode localCode = Util.GetNearlineLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetNearlineLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetPostLocalCode GetPostParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.GetPostLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.GetPostLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetPostResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetPostLocalCode localCode = Util.GetPostLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetPostLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetFollowlineLocalCode GetFollowlineParameterRegex(String page, String category_id, String value_id) {
            if (page != null) {
                if (!page.matches("^\\d{1,9}$")) {
                    return Util.GetFollowlineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED;
                }
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.GetFollowlineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value_id != null) {
                if (!value_id.matches("^\\d{1,9}$")) {
                    return Util.GetFollowlineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void GetFollowlineResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetFollowlineLocalCode localCode = Util.GetFollowlineLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetFollowlineLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetHeatmapLocalCode GetHeatmapParameterRegex() {
            return null;
        }

        @Override
        public void GetHeatmapResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetHeatmapLocalCode localCode = Util.GetHeatmapLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetHeatmapLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetFollower_RankLocalCode GetFollower_RankParameterRegex(String page) {
            if (page != null) {
                if (!page.matches("^\\d{1,9}$")) {
                    return Util.GetFollower_RankLocalCode.ERROR_PARAMETER_PAGE_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void GetFollower_RankResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetFollower_RankLocalCode localCode = Util.GetFollower_RankLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetFollower_RankLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetGochilineLocalCode GetGochilineParameterRegex(String page, String category_id, String value_id) {
            if (page != null) {
                if (!page.matches("^\\d{1,9}$")) {
                    return Util.GetGochilineLocalCode.ERROR_PARAMETER_PAGE_MALFORMED;
                }
            }
            if (category_id != null) {
                if (!category_id.matches("^\\d{1,9}$")) {
                    return Util.GetGochilineLocalCode.ERROR_PARAMETER_CATEGORY_ID_MALFORMED;
                }
            }
            if (value_id != null) {
                if (!value_id.matches("^\\d{1,9}$")) {
                    return Util.GetGochilineLocalCode.ERROR_PARAMETER_VALUE_ID_MALFORMED;
                }
            }
            return null;
        }

        @Override
        public void GetGochilineResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetGochilineLocalCode localCode = Util.GetGochilineLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetGochilineLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetFollowLocalCode GetFollowParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.GetFollowLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetFollowResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetFollowLocalCode localCode = Util.GetFollowLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetFollowLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetUser_CheerLocalCode GetUser_CheerParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.GetUser_CheerLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetUser_CheerResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetUser_CheerLocalCode localCode = Util.GetUser_CheerLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetUser_CheerLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetCommentLocalCode GetCommentParameterRegex(String post_id) {
            if (post_id != null) {
                if (!post_id.matches("^\\d{1,9}$")) {
                    return Util.GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MALFORMED;
                }
            } else {
                return Util.GetCommentLocalCode.ERROR_PARAMETER_POST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetCommentResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetCommentLocalCode localCode = Util.GetCommentLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetCommentLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetFollowerLocalCode GetFollowerParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.GetFollowerLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetFollowerResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetFollowerLocalCode localCode = Util.GetFollowerLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetFollowerLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetUserLocalCode GetUserParameterRegex(String user_id) {
            if (user_id != null) {
                if (!user_id.matches("^\\d{1,9}$")) {
                    return Util.GetUserLocalCode.ERROR_PARAMETER_USER_ID_MALFORMED;
                }
            } else {
                return Util.GetUserLocalCode.ERROR_PARAMETER_USER_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetUserResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetUserLocalCode localCode = Util.GetUserLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetUserLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetRestLocalCode GetRestParameterRegex(String rest_id) {
            if (rest_id != null) {
                if (!rest_id.matches("^\\d{1,9}$")) {
                    return Util.GetRestLocalCode.ERROR_PARAMETER_REST_ID_MALFORMED;
                }
            } else {
                return Util.GetRestLocalCode.ERROR_PARAMETER_REST_ID_MISSING;
            }
            return null;
        }

        @Override
        public void GetRestResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetRestLocalCode localCode = Util.GetRestLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetRestLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetNoticeLocalCode GetNoticeParameterRegex() {
            return null;
        }

        @Override
        public void GetNoticeResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetNoticeLocalCode localCode = Util.GetNoticeLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetNoticeLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

        @Override
        public Util.GetUsernameLocalCode GetUsernameParameterRegex(String username) {
            if (username != null) {
                if (!username.matches("^[^\\p{Cntrl}]{1,20}$")) {
                    return Util.GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MALFORMED;
                }
            } else {
                return Util.GetUsernameLocalCode.ERROR_PARAMETER_USERNAME_MISSING;
            }
            return null;
        }

        @Override
        public void GetUsernameResponse(JSONObject jsonObject, PayloadResponseCallback cb) {
            try {
                String version = jsonObject.getString("version");
                String uri = jsonObject.getString("uri");
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");

                Util.GlobalCode globalCode = Util.GlobalCodeReverseLookupTable(code);
                if (globalCode != null) {
                    if (globalCode == Util.GlobalCode.SUCCESS) {
                        JSONObject payload = jsonObject.getJSONObject("payload");
                        cb.onSuccess(payload);
                    } else {
                        cb.onGlobalError(globalCode);
                    }
                } else {
                    Util.GetUsernameLocalCode localCode = Util.GetUsernameLocalCodeReverseLookupTable(code);
                    if (localCode != null) {
                        String errorMessage = Util.GetUsernameLocalCodeMessageTable(localCode);
                        cb.onLocalError(errorMessage);
                    } else {
                        cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
                    }
                }
            } catch (JSONException e) {
                cb.onGlobalError(Util.GlobalCode.ERROR_UNKNOWN_ERROR);
            }
        }

    }
}