package com.inase.android.gocci.event;

/**
 * Created by kinagafuji on 15/12/12.
 */
public class AddressNameEvent {

    public String mPlace;

    public AddressNameEvent(String place) {
        super();
        this.mPlace = place;
    }
}
